<!doctype html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/icons/gelatoitaliano.ico">

    <title>Gelato Italiano - Datenschutz</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/gelatoitaliano.css">
</head>

<body>

<div class="fixed-top text-center">
    <div class="location-info pt-2 pb-2">
        <a class="link-light" href="https://goo.gl/maps/nY2AdR9GFTTk67Dp6">Du findest uns hier: Fliehburgstraße 15, 56856 Zell</a>
    </div>

    <nav class="navbar navbar-expand-lg navbar-light bg-light shadow-sm pt-3 mb-5 bg-white rounded aria-label="Navbar">
    <div class="container">
        <a class="navbar-brand" href="index.php">
            <img src="img/logo.svg" alt="Logo" width="250" height="135">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#gelatoitalianoNav" aria-controls="gelatoitalianoNav" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- NavBar Items -->
        <div class="collapse navbar-collapse" id="gelatoitalianoNav">
            <div class="nav-collapse-properties">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="eissorten.php">Unsere Eissorten</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="standort.php">Der Standort</a>
                    </li>
                </ul>
            </div>
        </div>

        <!-- Social Media logos -->
        <div class="colored-social-media-icon d-none d-xl-block d-lg-block padding-insta-logo">
            <a href="https://www.instagram.com/_gelatoitaliano_/">
                <img class="p-2" src="img/icons/instagram_white.svg" alt="Instagram" width="32" height="32">
            </a>
        </div>

        <div class="colored-social-media-icon d-none d-xl-block d-lg-block">
            <a href="https://www.facebook.com/Gelato-Italiano-100844725540830/">
                <img class="p-2" src="img/icons/facebook_white.svg" alt="Facebook" width="32" height="32">
            </a>
        </div>
    </div>
    </nav>
</div>

<!-- Hauptseite -->
<main role="main">
    <div class="container pt-4 pb-4">
        <h1 class="text-center pb-2">Datenschutz</h1>
        <h2>1. Datenschutz auf einen Blick</h2>
        <h3>Allgemeine Hinweise</h3>
        <p>Die folgenden Hinweise geben einen einfachen &Uuml;berblick dar&uuml;ber, was mit Ihren personenbezogenen Daten passiert, wenn Sie diese Website besuchen.
            Personenbezogene Daten sind alle Daten, mit denen Sie pers&ouml;nlich identifiziert werden k&ouml;nnen. Ausf&uuml;hrliche Informationen zum Thema
            Datenschutz entnehmen Sie unserer unter diesem Text aufgef&uuml;hrten Datenschutzerkl&auml;rung.</p>
        <h3>Datenerfassung auf dieser Website</h3> <h4>Wer ist verantwortlich f&uuml;r die Datenerfassung auf dieser Website?</h4>
        <p>Die Datenverarbeitung auf dieser Website erfolgt durch den Websitebetreiber. Dessen Kontaktdaten k&ouml;nnen Sie dem Abschnitt &bdquo;Hinweis zur
            Verantwortlichen Stelle&ldquo; in dieser Datenschutzerkl&auml;rung entnehmen.</p> <h4>Wie erfassen wir Ihre Daten?</h4>
        <p>Ihre Daten werden zum einen dadurch erhoben, dass Sie uns diese mitteilen. Hierbei kann es sich z.&nbsp;B. um Daten handeln, die Sie in ein
            Kontaktformular eingeben.</p>
        <p>Andere Daten werden automatisch oder nach Ihrer Einwilligung beim Besuch der Website durch unsere IT-Systeme erfasst. Das sind vor allem technische Daten
            (z.&nbsp;B. Internetbrowser, Betriebssystem oder Uhrzeit des Seitenaufrufs). Die Erfassung dieser Daten erfolgt automatisch, sobald Sie diese Website
            betreten.</p> <h4>Wof&uuml;r nutzen wir Ihre Daten?</h4>
        <p>Ein Teil der Daten wird erhoben, um eine fehlerfreie Bereitstellung der Website zu gew&auml;hrleisten. Andere Daten k&ouml;nnen zur Analyse Ihres
            Nutzerverhaltens verwendet werden.</p> <h4>Welche Rechte haben Sie bez&uuml;glich Ihrer Daten?</h4>
        <p>Sie haben jederzeit das Recht, unentgeltlich Auskunft &uuml;ber Herkunft, Empf&auml;nger und Zweck Ihrer gespeicherten personenbezogenen Daten zu
            erhalten. Sie haben au&szlig;erdem ein Recht, die Berichtigung oder L&ouml;schung dieser Daten zu verlangen. Wenn Sie eine Einwilligung zur
            Datenverarbeitung erteilt haben, k&ouml;nnen Sie diese Einwilligung jederzeit f&uuml;r die Zukunft widerrufen. Au&szlig;erdem haben Sie das Recht, unter
            bestimmten Umst&auml;nden die Einschr&auml;nkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen. Des Weiteren steht Ihnen ein
            Beschwerderecht bei der zust&auml;ndigen Aufsichtsbeh&ouml;rde zu.</p>
        <p>Hierzu sowie zu weiteren Fragen zum Thema Datenschutz k&ouml;nnen Sie sich jederzeit an uns wenden.</p>
        <h3>Analyse-Tools und Tools von Dritt&shy;anbietern</h3>
        <p>Beim Besuch dieser Website kann Ihr Surf-Verhalten statistisch ausgewertet werden. Das geschieht vor allem mit sogenannten Analyseprogrammen.</p>
        <p>Detaillierte Informationen zu diesen Analyseprogrammen finden Sie in der folgenden Datenschutzerkl&auml;rung.</p>
        <h2>2. Hosting und Content Delivery Networks (CDN)</h2>
        <h3>Externes Hosting</h3>
        <p>Diese Website wird bei einem externen Dienstleister gehostet (Hoster). Die personenbezogenen Daten, die auf dieser Website erfasst werden, werden auf den
            Servern des Hosters gespeichert. Hierbei kann es sich v. a. um IP-Adressen, Kontaktanfragen, Meta- und Kommunikationsdaten, Vertragsdaten, Kontaktdaten,
            Namen, Websitezugriffe und sonstige Daten, die &uuml;ber eine Website generiert werden, handeln.</p>
        <p>Der Einsatz des Hosters erfolgt zum Zwecke der Vertragserf&uuml;llung gegen&uuml;ber unseren potenziellen und bestehenden Kunden (Art. 6 Abs. 1 lit. b
            DSGVO) und im Interesse einer sicheren, schnellen und effizienten Bereitstellung unseres Online-Angebots durch einen professionellen Anbieter (Art. 6
            Abs. 1 lit. f DSGVO).</p>
        <p>Unser Hoster wird Ihre Daten nur insoweit verarbeiten, wie dies zur Erf&uuml;llung seiner Leistungspflichten erforderlich ist und unsere Weisungen in
            Bezug auf diese Daten befolgen.</p>
        <p>Wir setzen folgenden Hoster ein:</p>
        <p>Musterhosting AG<br/>
            Musterweg 100<br/>
            90210 Musterstadt</p>
        <h4>Abschluss eines Vertrages &uuml;ber Auftragsverarbeitung</h4>
        <p>Um die datenschutzkonforme Verarbeitung zu gew&auml;hrleisten, haben wir einen Vertrag &uuml;ber Auftragsverarbeitung mit unserem Hoster geschlossen.</p>
        <h2>3. Allgemeine Hinweise und Pflicht&shy;informationen</h2>
        <h3>Datenschutz</h3>
        <p>Die Betreiber dieser Seiten nehmen den Schutz Ihrer pers&ouml;nlichen Daten sehr ernst. Wir behandeln Ihre personenbezogenen Daten vertraulich und
            entsprechend der gesetzlichen Datenschutzvorschriften sowie dieser Datenschutzerkl&auml;rung.</p>
        <p>Wenn Sie diese Website benutzen, werden verschiedene personenbezogene Daten erhoben. Personenbezogene Daten sind Daten, mit denen Sie pers&ouml;nlich
            identifiziert werden k&ouml;nnen. Die vorliegende Datenschutzerkl&auml;rung erl&auml;utert, welche Daten wir erheben und wof&uuml;r wir sie nutzen. Sie
            erl&auml;utert auch, wie und zu welchem Zweck das geschieht.</p>
        <p>Wir weisen darauf hin, dass die Daten&uuml;bertragung im Internet (z.&nbsp;B. bei der Kommunikation per E-Mail) Sicherheitsl&uuml;cken aufweisen kann. Ein
            l&uuml;ckenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht m&ouml;glich.</p>
        <h3>Hinweis zur verantwortlichen Stelle</h3>
        <p>Die verantwortliche Stelle f&uuml;r die Datenverarbeitung auf dieser Website ist:</p>
        <p>Beispielfirma<br/>
            Musterweg 10<br/>
            90210 Musterstadt</p>

        <p>Telefon: +49 (0) 123 44 55 66<br/>
            E-Mail: info@beispielfirma.de</p>
        <p>Verantwortliche Stelle ist die nat&uuml;rliche oder juristische Person, die allein oder gemeinsam mit anderen &uuml;ber die Zwecke und Mittel der
            Verarbeitung von personenbezogenen Daten (z.&nbsp;B. Namen, E-Mail-Adressen o. &Auml;.) entscheidet.</p>

        <h3>Speicherdauer</h3>
        <p>Soweit innerhalb dieser Datenschutzerkl&auml;rung keine speziellere Speicherdauer genannt wurde, verbleiben Ihre personenbezogenen Daten bei uns, bis der
            Zweck f&uuml;r die Datenverarbeitung entf&auml;llt. Wenn Sie ein berechtigtes L&ouml;schersuchen geltend machen oder eine Einwilligung zur
            Datenverarbeitung widerrufen, werden Ihre Daten gel&ouml;scht, sofern wir keine anderen rechtlich zul&auml;ssigen Gr&uuml;nde f&uuml;r die Speicherung
            Ihrer personenbezogenen Daten haben (z.B. steuer- oder handelsrechtliche Aufbewahrungsfristen); im letztgenannten Fall erfolgt die L&ouml;schung nach
            Fortfall dieser Gr&uuml;nde.</p>
        <h3>Hinweis zur Datenweitergabe in die USA und sonstige Drittstaaten</h3>
        <p>Auf unserer Website sind unter anderem Tools von Unternehmen mit Sitz in den USA oder sonstigen datenschutzrechtlich nicht sicheren Drittstaaten
            eingebunden. Wenn diese Tools aktiv sind, k&ouml;nnen Ihre personenbezogene Daten in diese Drittstaaten &uuml;bertragen und dort verarbeitet werden. Wir
            weisen darauf hin, dass in diesen L&auml;ndern kein mit der EU vergleichbares Datenschutzniveau garantiert werden kann. Beispielsweise sind
            US-Unternehmen dazu verpflichtet, personenbezogene Daten an Sicherheitsbeh&ouml;rden herauszugeben, ohne dass Sie als Betroffener hiergegen gerichtlich
            vorgehen k&ouml;nnten. Es kann daher nicht ausgeschlossen werden, dass US-Beh&ouml;rden (z.B. Geheimdienste) Ihre auf US-Servern befindlichen Daten zu
            &Uuml;berwachungszwecken verarbeiten, auswerten und dauerhaft speichern. Wir haben auf diese Verarbeitungst&auml;tigkeiten keinen Einfluss.</p>
        <h3>Widerruf Ihrer Einwilligung zur Datenverarbeitung</h3>
        <p>Viele Datenverarbeitungsvorg&auml;nge sind nur mit Ihrer ausdr&uuml;cklichen Einwilligung m&ouml;glich. Sie k&ouml;nnen eine bereits erteilte Einwilligung
            jederzeit widerrufen. Die Rechtm&auml;&szlig;igkeit der bis zum Widerruf erfolgten Datenverarbeitung bleibt vom Widerruf unber&uuml;hrt.</p>
        <h3>Widerspruchsrecht gegen die Datenerhebung in besonderen F&auml;llen sowie gegen Direktwerbung (Art. 21 DSGVO)</h3>
        <p>WENN DIE DATENVERARBEITUNG AUF GRUNDLAGE VON ART. 6 ABS. 1 LIT. E ODER F DSGVO ERFOLGT, HABEN SIE JEDERZEIT DAS RECHT, AUS GR&Uuml;NDEN, DIE SICH AUS
            IHRER BESONDEREN SITUATION ERGEBEN, GEGEN DIE VERARBEITUNG IHRER PERSONENBEZOGENEN DATEN WIDERSPRUCH EINZULEGEN; DIES GILT AUCH F&Uuml;R EIN AUF DIESE
            BESTIMMUNGEN GEST&Uuml;TZTES PROFILING. DIE JEWEILIGE RECHTSGRUNDLAGE, AUF DENEN EINE VERARBEITUNG BERUHT, ENTNEHMEN SIE DIESER DATENSCHUTZERKL&Auml;RUNG.
            WENN SIE WIDERSPRUCH EINLEGEN, WERDEN WIR IHRE BETROFFENEN PERSONENBEZOGENEN DATEN NICHT MEHR VERARBEITEN, ES SEI DENN, WIR K&Ouml;NNEN ZWINGENDE SCHUTZW&Uuml;RDIGE
            GR&Uuml;NDE F&Uuml;R DIE VERARBEITUNG NACHWEISEN, DIE IHRE INTERESSEN, RECHTE UND FREIHEITEN &Uuml;BERWIEGEN ODER DIE VERARBEITUNG DIENT DER
            GELTENDMACHUNG, AUS&Uuml;BUNG ODER VERTEIDIGUNG VON RECHTSANSPR&Uuml;CHEN (WIDERSPRUCH NACH ART. 21 ABS. 1 DSGVO).</p>
        <p>WERDEN IHRE PERSONENBEZOGENEN DATEN VERARBEITET, UM DIREKTWERBUNG ZU BETREIBEN, SO HABEN SIE DAS RECHT, JEDERZEIT WIDERSPRUCH GEGEN DIE VERARBEITUNG SIE
            BETREFFENDER PERSONENBEZOGENER DATEN ZUM ZWECKE DERARTIGER WERBUNG EINZULEGEN; DIES GILT AUCH F&Uuml;R DAS PROFILING, SOWEIT ES MIT SOLCHER DIREKTWERBUNG
            IN VERBINDUNG STEHT. WENN SIE WIDERSPRECHEN, WERDEN IHRE PERSONENBEZOGENEN DATEN ANSCHLIESSEND NICHT MEHR ZUM ZWECKE DER DIREKTWERBUNG VERWENDET
            (WIDERSPRUCH NACH ART. 21 ABS. 2 DSGVO).</p>
        <h3>Beschwerde&shy;recht bei der zust&auml;ndigen Aufsichts&shy;beh&ouml;rde</h3>
        <p>Im Falle von Verst&ouml;&szlig;en gegen die DSGVO steht den Betroffenen ein Beschwerderecht bei einer Aufsichtsbeh&ouml;rde, insbesondere in dem
            Mitgliedstaat ihres gew&ouml;hnlichen Aufenthalts, ihres Arbeitsplatzes oder des Orts des mutma&szlig;lichen Versto&szlig;es zu. Das Beschwerderecht
            besteht unbeschadet anderweitiger verwaltungsrechtlicher oder gerichtlicher Rechtsbehelfe.</p>
        <h3>Recht auf Daten&shy;&uuml;bertrag&shy;barkeit</h3>
        <p>Sie haben das Recht, Daten, die wir auf Grundlage Ihrer Einwilligung oder in Erf&uuml;llung eines Vertrags automatisiert verarbeiten, an sich oder an
            einen Dritten in einem g&auml;ngigen, maschinenlesbaren Format aush&auml;ndigen zu lassen. Sofern Sie die direkte &Uuml;bertragung der Daten an einen
            anderen Verantwortlichen verlangen, erfolgt dies nur, soweit es technisch machbar ist.</p>
        <h3>SSL- bzw. TLS-Verschl&uuml;sselung</h3>
        <p>Diese Seite nutzt aus Sicherheitsgr&uuml;nden und zum Schutz der &Uuml;bertragung vertraulicher Inhalte, wie zum Beispiel Bestellungen oder Anfragen, die
            Sie an uns als Seitenbetreiber senden, eine SSL- bzw. TLS-Verschl&uuml;sselung. Eine verschl&uuml;sselte Verbindung erkennen Sie daran, dass die
            Adresszeile des Browsers von &bdquo;http://&ldquo; auf &bdquo;https://&ldquo; wechselt und an dem Schloss-Symbol in Ihrer Browserzeile.</p>
        <p>Wenn die SSL- bzw. TLS-Verschl&uuml;sselung aktiviert ist, k&ouml;nnen die Daten, die Sie an uns &uuml;bermitteln, nicht von Dritten mitgelesen
            werden.</p>
        <h3>Verschl&uuml;sselter Zahlungsverkehr auf dieser Website</h3>
        <p>Besteht nach dem Abschluss eines kostenpflichtigen Vertrags eine Verpflichtung, uns Ihre Zahlungsdaten (z.&nbsp;B. Kontonummer bei Einzugserm&auml;chtigung)
            zu &uuml;bermitteln, werden diese Daten zur Zahlungsabwicklung ben&ouml;tigt.</p>
        <p>Der Zahlungsverkehr &uuml;ber die g&auml;ngigen Zahlungsmittel (Visa/MasterCard, Lastschriftverfahren) erfolgt ausschlie&szlig;lich &uuml;ber eine verschl&uuml;sselte
            SSL- bzw. TLS-Verbindung. Eine verschl&uuml;sselte Verbindung erkennen Sie daran, dass die Adresszeile des Browsers von &bdquo;http://&ldquo; auf &bdquo;https://&ldquo;
            wechselt und an dem Schloss-Symbol in Ihrer Browserzeile.</p>
        <p>Bei verschl&uuml;sselter Kommunikation k&ouml;nnen Ihre Zahlungsdaten, die Sie an uns &uuml;bermitteln, nicht von Dritten mitgelesen werden.</p>
        <h3>Auskunft, L&ouml;schung und Berichtigung</h3>
        <p>Sie haben im Rahmen der geltenden gesetzlichen Bestimmungen jederzeit das Recht auf unentgeltliche Auskunft &uuml;ber Ihre gespeicherten personenbezogenen
            Daten, deren Herkunft und Empf&auml;nger und den Zweck der Datenverarbeitung und ggf. ein Recht auf Berichtigung oder L&ouml;schung dieser Daten. Hierzu
            sowie zu weiteren Fragen zum Thema personenbezogene Daten k&ouml;nnen Sie sich jederzeit an uns wenden.</p>
        <h3>Recht auf Einschr&auml;nkung der Verarbeitung</h3>
        <p>Sie haben das Recht, die Einschr&auml;nkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen. Hierzu k&ouml;nnen Sie sich jederzeit an uns
            wenden. Das Recht auf Einschr&auml;nkung der Verarbeitung besteht in folgenden F&auml;llen:</p>
        <ul>
            <li>Wenn Sie die Richtigkeit Ihrer bei uns gespeicherten personenbezogenen Daten bestreiten, ben&ouml;tigen wir in der Regel Zeit, um dies zu &uuml;berpr&uuml;fen.
                F&uuml;r die Dauer der Pr&uuml;fung haben Sie das Recht, die Einschr&auml;nkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen.
            </li>
            <li>Wenn die Verarbeitung Ihrer personenbezogenen Daten unrechtm&auml;&szlig;ig geschah/geschieht, k&ouml;nnen Sie statt der L&ouml;schung die Einschr&auml;nkung
                der Datenverarbeitung verlangen.
            </li>
            <li>Wenn wir Ihre personenbezogenen Daten nicht mehr ben&ouml;tigen, Sie sie jedoch zur Aus&uuml;bung, Verteidigung oder Geltendmachung von Rechtsanspr&uuml;chen
                ben&ouml;tigen, haben Sie das Recht, statt der L&ouml;schung die Einschr&auml;nkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen.
            </li>
            <li>Wenn Sie einen Widerspruch nach Art. 21 Abs. 1 DSGVO eingelegt haben, muss eine Abw&auml;gung zwischen Ihren und unseren Interessen vorgenommen
                werden. Solange noch nicht feststeht, wessen Interessen &uuml;berwiegen, haben Sie das Recht, die Einschr&auml;nkung der Verarbeitung Ihrer
                personenbezogenen Daten zu verlangen.
            </li>
        </ul>
        <p>Wenn Sie die Verarbeitung Ihrer personenbezogenen Daten eingeschr&auml;nkt haben, d&uuml;rfen diese Daten &ndash; von ihrer Speicherung abgesehen &ndash;
            nur mit Ihrer Einwilligung oder zur Geltendmachung, Aus&uuml;bung oder Verteidigung von Rechtsanspr&uuml;chen oder zum Schutz der Rechte einer anderen
            nat&uuml;rlichen oder juristischen Person oder aus Gr&uuml;nden eines wichtigen &ouml;ffentlichen Interesses der Europ&auml;ischen Union oder eines
            Mitgliedstaats verarbeitet werden.</p>
        <h3>Widerspruch gegen Werbe-E-Mails</h3>
        <p>Der Nutzung von im Rahmen der Impressumspflicht ver&ouml;ffentlichten Kontaktdaten zur &Uuml;bersendung von nicht ausdr&uuml;cklich angeforderter Werbung
            und Informationsmaterialien wird hiermit widersprochen. Die Betreiber der Seiten behalten sich ausdr&uuml;cklich rechtliche Schritte im Falle der
            unverlangten Zusendung von Werbeinformationen, etwa durch Spam-E-Mails, vor.</p>
        <h2>4. Datenerfassung auf dieser Website</h2>
        <h3>Cookies</h3>
        <p>Unsere Internetseiten verwenden so genannte &bdquo;Cookies&ldquo;. Cookies sind kleine Textdateien und richten auf Ihrem Endger&auml;t keinen Schaden an.
            Sie werden entweder vor&uuml;bergehend f&uuml;r die Dauer einer Sitzung (Session-Cookies) oder dauerhaft (permanente Cookies) auf Ihrem Endger&auml;t
            gespeichert. Session-Cookies werden nach Ende Ihres Besuchs automatisch gel&ouml;scht. Permanente Cookies bleiben auf Ihrem Endger&auml;t gespeichert,
            bis Sie diese selbst l&ouml;schen&nbsp;oder eine automatische L&ouml;schung durch Ihren Webbrowser erfolgt.</p>
        <p>Teilweise k&ouml;nnen auch Cookies von Drittunternehmen auf Ihrem Endger&auml;t gespeichert werden, wenn Sie unsere Seite betreten (Third-Party-Cookies).
            Diese erm&ouml;glichen uns oder Ihnen die Nutzung bestimmter Dienstleistungen des Drittunternehmens (z.B. Cookies zur Abwicklung von
            Zahlungsdienstleistungen).</p>
        <p>Cookies haben verschiedene Funktionen. Zahlreiche Cookies sind technisch notwendig, da bestimmte Websitefunktionen ohne diese nicht funktionieren w&uuml;rden
            (z.B. die Warenkorbfunktion oder die Anzeige von Videos). Andere Cookies dienen dazu, das Nutzerverhalten auszuwerten&nbsp;oder Werbung anzuzeigen.</p>
        <p>Cookies, die zur Durchf&uuml;hrung des elektronischen Kommunikationsvorgangs (notwendige Cookies) oder zur Bereitstellung bestimmter, von Ihnen erw&uuml;nschter
            Funktionen (funktionale Cookies, z. B. f&uuml;r die Warenkorbfunktion) oder zur Optimierung der Website (z.B. Cookies zur Messung des Webpublikums)
            erforderlich sind, werden auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO gespeichert, sofern keine andere Rechtsgrundlage angegeben wird. Der
            Websitebetreiber hat ein berechtigtes Interesse an der Speicherung von Cookies zur technisch fehlerfreien und optimierten Bereitstellung seiner Dienste.
            Sofern eine Einwilligung zur Speicherung von Cookies abgefragt wurde, erfolgt die Speicherung der betreffenden Cookies ausschlie&szlig;lich auf Grundlage
            dieser Einwilligung (Art. 6 Abs. 1 lit. a DSGVO); die Einwilligung ist jederzeit widerrufbar.</p>
        <p>Sie k&ouml;nnen Ihren Browser so einstellen, dass Sie &uuml;ber das Setzen von Cookies informiert werden und Cookies nur im Einzelfall erlauben, die
            Annahme von Cookies f&uuml;r bestimmte F&auml;lle oder generell ausschlie&szlig;en sowie das automatische L&ouml;schen der Cookies beim Schlie&szlig;en
            des Browsers aktivieren. Bei der Deaktivierung von Cookies kann die Funktionalit&auml;t dieser Website eingeschr&auml;nkt sein.</p>
        <p>Soweit Cookies von Drittunternehmen oder zu Analysezwecken eingesetzt werden, werden wir Sie hier&uuml;ber im Rahmen dieser Datenschutzerkl&auml;rung
            gesondert informieren und ggf. eine Einwilligung abfragen.</p>
        <h3>Cookie-Einwilligung mit Borlabs Cookie</h3>
        <p>Unsere Website nutzt die Cookie-Consent-Technologie von Borlabs Cookie, um Ihre Einwilligung zur Speicherung bestimmter Cookies in Ihrem Browser
            einzuholen und diese datenschutzkonform zu dokumentieren. Anbieter dieser Technologie ist Borlabs - Benjamin A. Bornschein, R&uuml;benkamp 32, 22305
            Hamburg (im Folgenden Borlabs).</p>
        <p>Wenn Sie unsere Website betreten, wird ein Borlabs-Cookie in Ihrem Browser gespeichert, in dem die von Ihnen erteilten Einwilligungen oder der Widerruf
            dieser Einwilligungen gespeichert werden. Diese Daten werden nicht an den Anbieter von Borlabs Cookie weitergegeben.</p>
        <p>Die erfassten Daten werden gespeichert, bis Sie uns zur L&ouml;schung auffordern bzw. das Borlabs-Cookie selbst l&ouml;schen oder der Zweck f&uuml;r die
            Datenspeicherung entf&auml;llt. Zwingende gesetzliche Aufbewahrungsfristen bleiben unber&uuml;hrt. Details zur Datenverarbeitung von Borlabs Cookie
            finden Sie unter <a href="https://de.borlabs.io/kb/welche-daten-speichert-borlabs-cookie/" target="_blank" rel="noopener noreferrer">https://de.borlabs.io/kb/welche-daten-speichert-borlabs-cookie/</a>.
        </p>
        <p>Der Einsatz der Borlabs-Cookie-Consent-Technologie erfolgt, um die gesetzlich vorgeschriebenen Einwilligungen f&uuml;r den Einsatz von Cookies einzuholen.
            Rechtsgrundlage hierf&uuml;r ist Art. 6 Abs. 1 lit. c DSGVO.</p>
        <h3>Cookie-Einwilligung mit Consent Manager Provider</h3>
        <p>Unsere Website nutzt die Cookie-Consent-Technologie von Consent Manager Provider, um Ihre Einwilligung zur Speicherung bestimmter Cookies auf Ihrem Endger&auml;t
            einzuholen und diese datenschutzkonform zu dokumentieren. Anbieter dieser Technologie ist die Jaohawi AB, H&aring;ltegelv&auml;gen 1b, 72348 V&auml;ster&aring;s,
            Schweden, Website: <a href="https://www.consentmanager.de" target="_blank" rel="noopener noreferrer">https://www.consentmanager.de</a> (im Folgenden
            &bdquo;Consent Manager Provider&ldquo;).</p>
        <p>Wenn Sie unsere Website betreten, wird eine Verbindung zu den Servern von Consent Manager Provider hergestellt, um Ihre Einwilligungen und sonstigen Erkl&auml;rungen
            zur Cookie-Nutzung einzuholen. Anschlie&szlig;end speichert Consent Manager Provider ein Cookie in Ihrem Browser, um Ihnen die erteilten Einwilligungen
            bzw. deren Widerruf zuordnen zu k&ouml;nnen. Die so erfassten Daten werden gespeichert, bis Sie uns zur L&ouml;schung auffordern, den
            Consent-Manager-Provider-Cookie selbst l&ouml;schen oder der Zweck f&uuml;r die Datenspeicherung entf&auml;llt. Zwingende gesetzliche
            Aufbewahrungspflichten bleiben unber&uuml;hrt.</p>
        <p>Der Einsatz von Consent Manager Provider erfolgt, um die gesetzlich vorgeschriebenen Einwilligungen f&uuml;r den Einsatz von Cookies einzuholen.
            Rechtsgrundlage hierf&uuml;r ist Art. 6 Abs. 1 lit. c DSGVO.</p>
        <h4>Vertrag &uuml;ber Auftragsverarbeitung</h4>
        <p>Wir haben einen Vertrag &uuml;ber Auftragsverarbeitung mit Consent Manager Provider geschlossen. Hierbei handelt es sich um einen datenschutzrechtlich
            vorgeschriebenen Vertrag, der gew&auml;hrleistet, dass Consent Manager Provider die personenbezogenen Daten unserer Websitebesucher nur nach unseren
            Weisungen und unter Einhaltung der DSGVO verarbeitet.</p>
        <h3>Cookie-Einwilligung mit Osano</h3>
        <p>Unsere Website nutzt die Cookie-Consent-Technologie von Osano, um Ihre Einwilligung zur Speicherung bestimmter Cookies auf Ihrem Endger&auml;t einzuholen
            und diese datenschutzkonform zu dokumentieren. Anbieter dieser Technologie ist Osano, Inc., 3800 North Lamar Blvd, Suite 200, Austin, Texas 78756, USA
            (im Folgenden &bdquo;Osano&ldquo;).</p>
        <p>Wenn Sie unsere Website betreten, wird eine Verbindung zu den Servern von Osano hergestellt, um Ihre Einwilligungen und sonstigen Erkl&auml;rungen zur
            Cookie-Nutzung einzuholen. Anschlie&szlig;end speichert Osano einen Cookie in Ihrem Browser, um Ihnen die erteilten Einwilligungen bzw. deren Widerruf
            zuordnen zu k&ouml;nnen. Die so erfassten Daten werden gespeichert, bis Sie uns zur L&ouml;schung auffordern, den Osano-Cookie selbst l&ouml;schen oder
            der Zweck f&uuml;r die Datenspeicherung entf&auml;llt. Zwingende gesetzliche Aufbewahrungspflichten bleiben unber&uuml;hrt.</p>
        <p>Laut Osano verbleiben die Daten von europ&auml;ischen Websitebesuchern in der EU, indem sie ausschlie&szlig;lich auf regionalen Servern verarbeitet
            werden.</p>
        <p>Der Einsatz von Osano erfolgt, um die gesetzlich vorgeschriebenen Einwilligungen f&uuml;r den Einsatz von Cookies einzuholen. Rechtsgrundlage hierf&uuml;r
            ist Art. 6 Abs. 1 lit. c DSGVO.</p>
        <h4>Vertrag &uuml;ber Auftragsverarbeitung</h4>
        <p>Wir haben einen Vertrag &uuml;ber Auftragsverarbeitung mit Osano geschlossen. Hierbei handelt es sich um einen datenschutzrechtlich vorgeschriebenen
            Vertrag, der gew&auml;hrleistet, dass Osano die personenbezogenen Daten unserer Websitebesucher nur nach unseren Weisungen und unter Einhaltung der DSGVO
            verarbeitet.</p>
        <h3>Server-Log-Dateien</h3>
        <p>Der Provider der Seiten erhebt und speichert automatisch Informationen in so genannten Server-Log-Dateien, die Ihr Browser automatisch an uns &uuml;bermittelt.
            Dies sind:</p>
        <ul>
            <li>Browsertyp und Browserversion</li>
            <li>verwendetes Betriebssystem</li>
            <li>Referrer URL</li>
            <li>Hostname des zugreifenden Rechners</li>
            <li>Uhrzeit der Serveranfrage</li>
            <li>IP-Adresse</li>
        </ul>
        <p>Eine Zusammenf&uuml;hrung dieser Daten mit anderen Datenquellen wird nicht vorgenommen.</p>
        <p>Die Erfassung dieser Daten erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an der technisch
            fehlerfreien Darstellung und der Optimierung seiner Website &ndash; hierzu m&uuml;ssen die Server-Log-Files erfasst werden.</p>
        <h3>Kontaktformular</h3>
        <p>Wenn Sie uns per Kontaktformular Anfragen zukommen lassen, werden Ihre Angaben aus dem Anfrageformular inklusive der von Ihnen dort angegebenen
            Kontaktdaten zwecks Bearbeitung der Anfrage und f&uuml;r den Fall von Anschlussfragen bei uns gespeichert. Diese Daten geben wir nicht ohne Ihre
            Einwilligung weiter.</p>
        <p>Die Verarbeitung dieser Daten erfolgt auf Grundlage von Art. 6 Abs. 1 lit. b DSGVO, sofern Ihre Anfrage mit der Erf&uuml;llung eines Vertrags zusammenh&auml;ngt
            oder zur Durchf&uuml;hrung vorvertraglicher Ma&szlig;nahmen erforderlich ist. In allen &uuml;brigen F&auml;llen beruht die Verarbeitung auf unserem
            berechtigten Interesse an der effektiven Bearbeitung der an uns gerichteten Anfragen (Art. 6 Abs. 1 lit. f DSGVO) oder auf Ihrer Einwilligung (Art. 6
            Abs. 1 lit. a DSGVO) sofern diese abgefragt wurde.</p>
        <p>Die von Ihnen im Kontaktformular eingegebenen Daten verbleiben bei uns, bis Sie uns zur L&ouml;schung auffordern, Ihre Einwilligung zur Speicherung
            widerrufen oder der Zweck f&uuml;r die Datenspeicherung entf&auml;llt (z.&nbsp;B. nach abgeschlossener Bearbeitung Ihrer Anfrage). Zwingende gesetzliche
            Bestimmungen &ndash; insbesondere Aufbewahrungsfristen &ndash; bleiben unber&uuml;hrt.</p>
        <h3>Anfrage per E-Mail, Telefon oder Telefax</h3>
        <p>Wenn Sie uns per E-Mail, Telefon oder Telefax kontaktieren, wird Ihre Anfrage inklusive aller daraus hervorgehenden personenbezogenen Daten (Name,
            Anfrage) zum Zwecke der Bearbeitung Ihres Anliegens bei uns gespeichert und verarbeitet. Diese Daten geben wir nicht ohne Ihre Einwilligung weiter.</p>
        <p>Die Verarbeitung dieser Daten erfolgt auf Grundlage von Art. 6 Abs. 1 lit. b DSGVO, sofern Ihre Anfrage mit der Erf&uuml;llung eines Vertrags zusammenh&auml;ngt
            oder zur Durchf&uuml;hrung vorvertraglicher Ma&szlig;nahmen erforderlich ist. In allen &uuml;brigen F&auml;llen beruht die Verarbeitung auf unserem
            berechtigten Interesse an der effektiven Bearbeitung der an uns gerichteten Anfragen (Art. 6 Abs. 1 lit. f DSGVO) oder auf Ihrer Einwilligung (Art. 6
            Abs. 1 lit. a DSGVO) sofern diese abgefragt wurde.</p>
        <p>Die von Ihnen an uns per Kontaktanfragen &uuml;bersandten Daten verbleiben bei uns, bis Sie uns zur L&ouml;schung auffordern, Ihre Einwilligung zur
            Speicherung widerrufen oder der Zweck f&uuml;r die Datenspeicherung entf&auml;llt (z.&nbsp;B. nach abgeschlossener Bearbeitung Ihres Anliegens).
            Zwingende gesetzliche Bestimmungen &ndash; insbesondere gesetzliche Aufbewahrungsfristen &ndash; bleiben unber&uuml;hrt.</p>
        <h3>Kommunikation via WhatsApp</h3>
        <p>F&uuml;r die Kommunikation mit unseren Kunden und sonstigen Dritten nutzen wir unter anderem den Instant-Messaging-Dienst WhatsApp. Anbieter ist die
            WhatsApp Ireland Limited, 4 Grand Canal Square, Grand Canal Harbour, Dublin 2, Irland.</p>
        <p>Die Kommunikation erfolgt &uuml;ber eine Ende-zu-Ende-Verschl&uuml;sselung (Peer-to-Peer), die verhindert, dass WhatsApp oder sonstige Dritte Zugriff auf
            die Kommunikationsinhalte erlangen k&ouml;nnen. WhatsApp erh&auml;lt jedoch Zugriff auf Metadaten, die im Zuge des Kommunikationsvorgangs entstehen (z.B.
            Absender, Empf&auml;nger und Zeitpunkt). Wir weisen ferner darauf hin, dass WhatsApp nach eigener Aussage, personenbezogene Daten seiner Nutzer mit
            seiner in den USA ans&auml;ssigen Konzernmutter Facebook teilt. Weitere Details zur Datenverarbeitung finden Sie in der Datenschutzrichtlinie von
            WhatsApp unter: <a href="https://www.whatsapp.com/legal/#privacy-policy" target="_blank" rel="noopener noreferrer">https://www.whatsapp.com/legal/#privacy-policy</a>.
        </p>
        <p>Der Einsatz von WhatsApp erfolgt auf Grundlage unseres berechtigten Interesses an einer m&ouml;glichst schnellen und effektiven Kommunikation mit Kunden,
            Interessenten und sonstigen Gesch&auml;fts- und Vertragspartnern (Art. 6 Abs. 1 lit. f DSGVO). Sofern eine entsprechende Einwilligung abgefragt wurde,
            erfolgt die Datenverarbeitung ausschlie&szlig;lich auf Grundlage der Einwilligung; diese ist jederzeit mit Wirkung f&uuml;r die Zukunft widerrufbar.</p>
        <p>Die zwischen und auf WhatsApp ausgetauschten Kommunikationsinhalte verbleiben bei uns, bis Sie uns zur L&ouml;schung auffordern, Ihre Einwilligung zur
            Speicherung widerrufen oder der Zweck f&uuml;r die Datenspeicherung entf&auml;llt (z.&nbsp;B. nach abgeschlossener Bearbeitung Ihrer Anfrage). Zwingende
            gesetzliche Bestimmungen &ndash; insbesondere Aufbewahrungsfristen &ndash; bleiben unber&uuml;hrt.</p>
        <p>Wir nutzen WhatsApp in der Variante &bdquo;WhatsApp Business&ldquo;.</p>
        <p>Die Daten&uuml;bertragung in die USA wird auf die Standardvertragsklauseln der EU-Kommission gest&uuml;tzt. Details finden Sie hier: <a
                href="https://www.whatsapp.com/legal/business-data-transfer-addendum" target="_blank" rel="noopener noreferrer">https://www.whatsapp.com/legal/business-data-transfer-addendum</a>.
        </p>
        <p>Wir haben unsere WhatsApp-Accounts so eingestellt, dass es keinen automatischen Datenabgleich mit dem Adressbuch auf den im Einsatz befindlichen
            Smartphones macht.</p>
        <p>Wir haben einen Vertrag &uuml;ber Auftragsverarbeitung mit WhatsApp geschlossen.</p>
        <h3>Registrierung auf dieser Website</h3>
        <p>Sie k&ouml;nnen sich auf dieser Website registrieren, um zus&auml;tzliche Funktionen auf der Seite zu nutzen. Die dazu eingegebenen Daten verwenden wir
            nur zum Zwecke der Nutzung des jeweiligen Angebotes oder Dienstes, f&uuml;r den Sie sich registriert haben. Die bei der Registrierung abgefragten
            Pflichtangaben m&uuml;ssen vollst&auml;ndig angegeben werden. Anderenfalls werden wir die Registrierung ablehnen.</p>
        <p>F&uuml;r wichtige &Auml;nderungen etwa beim Angebotsumfang oder bei technisch notwendigen &Auml;nderungen nutzen wir die bei der Registrierung angegebene
            E-Mail-Adresse, um Sie auf diesem Wege zu informieren.</p>
        <p>Die Verarbeitung der bei der Registrierung eingegebenen Daten erfolgt zum Zwecke der Durchf&uuml;hrung des durch die Registrierung begr&uuml;ndeten
            Nutzungsverh&auml;ltnisses und ggf. zur Anbahnung weiterer Vertr&auml;ge (Art. 6 Abs. 1 lit. b DSGVO).</p>
        <p>Die bei der Registrierung erfassten Daten werden von uns gespeichert, solange Sie auf dieser Website registriert sind und werden anschlie&szlig;end gel&ouml;scht.
            Gesetzliche Aufbewahrungsfristen bleiben unber&uuml;hrt.</p>
        <h3>Registrierung mit Google</h3>
        <p>Statt einer direkten Registrierung auf dieser Website k&ouml;nnen Sie sich mit Google registrieren. Anbieter dieses Dienstes ist die Google Ireland
            Limited (&bdquo;Google&rdquo;), Gordon House, Barrow Street, Dublin 4, Irland.</p>
        <p>Um sich mit Google zu registrieren, m&uuml;ssen Sie ausschlie&szlig;lich Ihre Google-Namen und Ihr Passwort eingeben. Google wird Sie identifizieren und
            unserer Website Ihre Identit&auml;t best&auml;tigen.</p>
        <p>Wenn Sie sich mit Google anmelden, ist es uns ggf. m&ouml;glich bestimmte Informationen auf Ihrem Konto zu nutzen, um Ihr Profil bei uns zu vervollst&auml;ndigen.
            Ob und welche Informationen das sind, entscheiden Sie im Rahmen Ihrer Google-Sicherheitseinstellungen, die Sie hier finden: <a
                href="https://myaccount.google.com/security" target="_blank" rel="noopener noreferrer">https://myaccount.google.com/security</a> und <a
                href="https://myaccount.google.com/permissions" target="_blank" rel="noopener noreferrer">https://myaccount.google.com/permissions</a>.</p>
        <p>Die Datenverarbeitung, die mit der Google-Registrierung einhergeht beruht auf unserem berechtigten Interesse, unseren Nutzern einen m&ouml;glichst
            einfachen Registrierungsprozess zu erm&ouml;glichen (Art. 6 Abs. 1 lit. f DSGVO). Da die Nutzung der Registrierungsfunktion freiwillig ist und die Nutzer
            selbst &uuml;ber die jeweiligen Zugriffsm&ouml;glichkeiten entscheiden k&ouml;nnen, sind keine entgegenstehenden &uuml;berwiegenden Rechte der
            Betroffenen ersichtlich.</p>
        <h3>Registrierung mit Facebook Connect</h3>
        <p>Statt einer direkten Registrierung auf dieser Website k&ouml;nnen Sie sich mit Facebook Connect registrieren. Anbieter dieses Dienstes ist die Facebook
            Ireland Limited, 4 Grand Canal Square, Dublin 2, Irland. Die erfassten Daten werden nach Aussage von Facebook jedoch auch in die USA und in andere Drittl&auml;nder
            &uuml;bertragen.</p>
        <p>Wenn Sie sich f&uuml;r die Registrierung mit Facebook Connect entscheiden und auf den &bdquo;Login with Facebook&rdquo;-/&bdquo;Connect with Facebook&rdquo;-Button
            klicken, werden Sie automatisch auf die Plattform von Facebook weitergeleitet. Dort k&ouml;nnen Sie sich mit Ihren Nutzungsdaten anmelden. Dadurch wird
            Ihr Facebook-Profil mit dieser Website bzw. unseren Diensten verkn&uuml;pft. Durch diese Verkn&uuml;pfung erhalten wir Zugriff auf Ihre bei Facebook
            hinterlegten Daten. Dies sind vor allem:</p>
        <ul>
            <li>Facebook-Name</li>
            <li>Facebook-Profil- und Titelbild</li>
            <li>Facebook-Titelbild</li>
            <li>bei Facebook hinterlegte E-Mail-Adresse</li>
            <li>Facebook-ID</li>
            <li>Facebook-Freundeslisten</li>
            <li>Facebook Likes (&bdquo;Gef&auml;llt-mir&ldquo;-Angaben)</li>
            <li>Geburtstag</li>
            <li>Geschlecht</li>
            <li>Land</li>
            <li>Sprache</li>
        </ul>
        <p>Diese Daten werden zur Einrichtung, Bereitstellung und Personalisierung Ihres Accounts genutzt.</p>
        <p>Die Registrierung mit Facebook-Connect und die damit verbundenen Datenverarbeitungsvorg&auml;nge erfolgen auf Grundlage Ihrer Einwilligung (Art. 6 Abs. 1
            lit. a DSGVO). Diese Einwilligung k&ouml;nnen Sie jederzeit mit Wirkung f&uuml;r die Zukunft widerrufen.</p>
        <p>Soweit mit Hilfe des hier beschriebenen Tools personenbezogene Daten auf unserer Website erfasst und an Facebook weitergeleitet werden, sind wir und die
            Facebook Ireland Limited, 4 Grand Canal Square, Grand Canal Harbour, Dublin 2, Irland gemeinsam f&uuml;r diese Datenverarbeitung verantwortlich (Art. 26
            DSGVO). Die gemeinsame Verantwortlichkeit beschr&auml;nkt sich dabei ausschlie&szlig;lich auf die Erfassung der Daten und deren Weitergabe an Facebook.
            Die nach der Weiterleitung erfolgende Verarbeitung durch Facebook ist nicht Teil der gemeinsamen Verantwortung. Die uns gemeinsam obliegenden
            Verpflichtungen wurden in einer Vereinbarung &uuml;ber gemeinsame Verarbeitung festgehalten. Den Wortlaut der Vereinbarung finden Sie unter: <a
                href="https://www.facebook.com/legal/controller_addendum" target="_blank" rel="noopener noreferrer">https://www.facebook.com/legal/controller_addendum</a>.
            Laut dieser Vereinbarung sind wir f&uuml;r die Erteilung der Datenschutzinformationen beim Einsatz des Facebook-Tools und f&uuml;r die
            datenschutzrechtlich sichere Implementierung des Tools auf unserer Website verantwortlich. F&uuml;r die Datensicherheit der Facebook-Produkte ist
            Facebook verantwortlich. Betroffenenrechte (z.B. Auskunftsersuchen) hinsichtlich der bei Facebook verarbeiteten Daten k&ouml;nnen Sie direkt bei Facebook
            geltend machen. Wenn Sie die Betroffenenrechte bei uns geltend machen, sind wir verpflichtet, diese an Facebook weiterzuleiten.</p>
        <p>Die Daten&uuml;bertragung in die USA wird auf die Standardvertragsklauseln der EU-Kommission gest&uuml;tzt. Details finden Sie hier: <a
                href="https://www.facebook.com/legal/EU_data_transfer_addendum" target="_blank" rel="noopener noreferrer">https://www.facebook.com/legal/EU_data_transfer_addendum</a>,
            <a href="https://de-de.facebook.com/help/566994660333381" target="_blank" rel="noopener noreferrer">https://de-de.facebook.com/help/566994660333381</a>
            und <a href="https://www.facebook.com/policy.php" target="_blank" rel="noopener noreferrer">https://www.facebook.com/policy.php</a>.</p>
        <p>Weitere Informationen finden Sie in den Facebook-Nutzungsbedingungen und den Facebook-Datenschutzbestimmungen. Diese finden Sie unter: <a
                href="https://de-de.facebook.com/about/privacy/" target="_blank" rel="noopener noreferrer">https://de-de.facebook.com/about/privacy/</a> und <a
                href="https://de-de.facebook.com/legal/terms/" target="_blank" rel="noopener noreferrer">https://de-de.facebook.com/legal/terms/</a>.</p>
        <h3>ProvenExpert</h3>
        <p>Wir haben Bewertungssiegel von ProvenExpert auf dieser Website eingebunden. Anbieter ist Expert Systems AG, Quedlinburger Str. 1, 10589 Berlin, <a
                href="https://www.provenexpert.com" target="_blank" rel="noopener noreferrer">https://www.provenexpert.com</a>.</p>
        <p>Das ProvenExpert-Siegel erm&ouml;glicht es uns, Kundenbewertungen, die bei ProvenExpert zu unserem Unternehmen abgegeben wurden, auf unserer Website in
            einem Siegel darzustellen. Wenn Sie unsere Website besuchen, wird eine Verbindung mit ProvenExpert hergestellt, sodass ProvenExpert feststellen kann,
            dass Sie unsere Website besucht haben. Ferner erfasst ProvenExpert Ihre Spracheinstellungen, um das Siegel in der gew&auml;hlten Landessprache
            anzuzeigen.</p>
        <p>Die Verwendung von ProvenExpert erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an einer m&ouml;glichst
            nachvollziehbaren Darstellung von Kundenbewertungen. Sofern eine entsprechende Einwilligung abgefragt wurde, erfolgt die Verarbeitung ausschlie&szlig;lich
            auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit widerrufbar.</p>
        <h2>5. Soziale Medien</h2>
        <h3>eRecht24 Safe Sharing Tool</h3>
        <p>Die Inhalte auf dieser Website k&ouml;nnen datenschutzkonform in sozialen Netzwerken wie Facebook, Twitter &amp; Co. geteilt werden. Diese Seite nutzt daf&uuml;r
            das <a href="https://www.e-recht24.de/erecht24-safe-sharing.html" target="_blank" rel="noopener noreferrer">eRecht24 Safe Sharing Tool</a>. Dieses Tool
            stellt den direkten Kontakt zwischen den Netzwerken und Nutzern erst dann her, wenn der Nutzer aktiv auf einen dieser Button klickt. Der Klick auf den
            Button stellt eine Einwilligung im Sinne des Art. 6 Abs. 1 lit. a DSGVO dar. Diese Einwilligung kann jederzeit mit Wirkung f&uuml;r die Zukunft
            widerrufen werden.</p>
        <p>Eine automatische &Uuml;bertragung von Nutzerdaten an die Betreiber dieser Plattformen erfolgt durch dieses Tool nicht. Ist der Nutzer bei einem der
            sozialen Netzwerke angemeldet, erscheint bei der Nutzung der Social-Buttons von Facebook, Twitter &amp; Co. ein Informations-Fenster, in dem der Nutzer
            den Text vor dem Absenden best&auml;tigen kann.</p>
        <p>Unsere Nutzer k&ouml;nnen die Inhalte dieser Seite datenschutzkonform in sozialen Netzwerken teilen, ohne dass komplette Surf-Profile durch die Betreiber
            der Netzwerke erstellt werden.</p>
        <h3>Social-Media-Plugins mit Shariff</h3>
        <p>Auf dieser Website werden Plugins von sozialen Medien verwendet (z.&nbsp;B. Facebook, Twitter, Instagram, Pinterest, XING, LinkedIn, Tumblr).</p>
        <p>Die Plugins k&ouml;nnen Sie in der Regel anhand der jeweiligen Social-Media-Logos erkennen. Um den Datenschutz auf dieser Website zu gew&auml;hrleisten,
            verwenden wir diese Plugins nur zusammen mit der sogenannten &bdquo;Shariff&ldquo;-L&ouml;sung. Diese Anwendung verhindert, dass die auf dieser Website
            integrierten Plugins Daten schon beim ersten Betreten der Seite an den jeweiligen Anbieter &uuml;bertragen.</p>
        <p>Erst wenn Sie das jeweilige Plugin durch Anklicken der zugeh&ouml;rigen Schaltfl&auml;che aktivieren, wird eine direkte Verbindung zum Server des
            Anbieters hergestellt (Einwilligung). Sobald Sie das Plugin aktivieren, erh&auml;lt der jeweilige Anbieter die Information, dass Sie mit Ihrer IP-Adresse
            dieser Website besucht haben. Wenn Sie gleichzeitig in Ihrem jeweiligen Social-Media-Account (z.&nbsp;B. Facebook) eingeloggt sind, kann der jeweilige
            Anbieter den Besuch dieser Website Ihrem Benutzerkonto zuordnen.</p>
        <p>Das Aktivieren des Plugins stellt eine Einwilligung im Sinne des Art. 6 Abs. 1 lit. a DSGVO dar. Diese Einwilligung k&ouml;nnen Sie jederzeit mit Wirkung
            f&uuml;r die Zukunft widerrufen.</p>
        <h3>Facebook Plugins (Like &amp; Share-Button)</h3>
        <p>Auf dieser Website sind Plugins des sozialen Netzwerks Facebook integriert. Anbieter dieses Dienstes ist die Facebook Ireland Limited, 4 Grand Canal
            Square, Dublin 2, Irland. Die erfassten Daten werden nach Aussage von Facebook jedoch auch in die USA und in andere Drittl&auml;nder &uuml;bertragen.</p>
        <p>Die Facebook Plugins erkennen Sie an dem Facebook-Logo oder dem &bdquo;Like-Button&ldquo; (&bdquo;Gef&auml;llt mir&ldquo;) auf dieser Website. Eine &Uuml;bersicht
            &uuml;ber die Facebook Plugins finden Sie hier: <a href="https://developers.facebook.com/docs/plugins/?locale=de_DE" target="_blank"
                                                               rel="noopener noreferrer">https://developers.facebook.com/docs/plugins/?locale=de_DE</a>.</p>
        <p>Wenn Sie diese Website besuchen, wird &uuml;ber das Plugin eine direkte Verbindung zwischen Ihrem Browser und dem Facebook-Server hergestellt. Facebook
            erh&auml;lt dadurch die Information, dass Sie mit Ihrer IP-Adresse diese Website besucht haben. Wenn Sie den Facebook &bdquo;Like-Button&ldquo;
            anklicken, w&auml;hrend Sie in Ihrem Facebook-Account eingeloggt sind, k&ouml;nnen Sie die Inhalte dieser Website auf Ihrem Facebook-Profil verlinken.
            Dadurch kann Facebook den Besuch dieser Website Ihrem Benutzerkonto zuordnen. Wir weisen darauf hin, dass wir als Anbieter der Seiten keine Kenntnis vom
            Inhalt der &uuml;bermittelten Daten sowie deren Nutzung durch Facebook erhalten. Weitere Informationen hierzu finden Sie in der Datenschutzerkl&auml;rung
            von Facebook unter: <a href="https://de-de.facebook.com/privacy/explanation" target="_blank" rel="noopener noreferrer">https://de-de.facebook.com/privacy/explanation</a>.
        </p>
        <p>Wenn Sie nicht w&uuml;nschen, dass Facebook den Besuch dieser Website Ihrem Facebook-Nutzerkonto zuordnen kann, loggen Sie sich bitte aus Ihrem
            Facebook-Benutzerkonto aus.</p>
        <p>Die Verwendung der Facebook Plugins erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an einer m&ouml;glichst
            umfangreichen Sichtbarkeit in den Sozialen Medien. Sofern eine entsprechende Einwilligung abgefragt wurde, erfolgt die Verarbeitung ausschlie&szlig;lich
            auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit widerrufbar.</p>
        <p>Soweit mit Hilfe des hier beschriebenen Tools personenbezogene Daten auf unserer Website erfasst und an Facebook weitergeleitet werden, sind wir und die
            Facebook Ireland Limited, 4 Grand Canal Square, Grand Canal Harbour, Dublin 2, Irland gemeinsam f&uuml;r diese Datenverarbeitung verantwortlich (Art. 26
            DSGVO). Die gemeinsame Verantwortlichkeit beschr&auml;nkt sich dabei ausschlie&szlig;lich auf die Erfassung der Daten und deren Weitergabe an Facebook.
            Die nach der Weiterleitung erfolgende Verarbeitung durch Facebook ist nicht Teil der gemeinsamen Verantwortung. Die uns gemeinsam obliegenden
            Verpflichtungen wurden in einer Vereinbarung &uuml;ber gemeinsame Verarbeitung festgehalten. Den Wortlaut der Vereinbarung finden Sie unter: <a
                href="https://www.facebook.com/legal/controller_addendum" target="_blank" rel="noopener noreferrer">https://www.facebook.com/legal/controller_addendum</a>.
            Laut dieser Vereinbarung sind wir f&uuml;r die Erteilung der Datenschutzinformationen beim Einsatz des Facebook-Tools und f&uuml;r die
            datenschutzrechtlich sichere Implementierung des Tools auf unserer Website verantwortlich. F&uuml;r die Datensicherheit der Facebook-Produkte ist
            Facebook verantwortlich. Betroffenenrechte (z.B. Auskunftsersuchen) hinsichtlich der bei Facebook verarbeiteten Daten k&ouml;nnen Sie direkt bei Facebook
            geltend machen. Wenn Sie die Betroffenenrechte bei uns geltend machen, sind wir verpflichtet, diese an Facebook weiterzuleiten.</p>
        <p>Die Daten&uuml;bertragung in die USA wird auf die Standardvertragsklauseln der EU-Kommission gest&uuml;tzt. Details finden Sie hier: <a
                href="https://www.facebook.com/legal/EU_data_transfer_addendum" target="_blank" rel="noopener noreferrer">https://www.facebook.com/legal/EU_data_transfer_addendum</a>,
            <a href="https://de-de.facebook.com/help/566994660333381" target="_blank" rel="noopener noreferrer">https://de-de.facebook.com/help/566994660333381</a>
            und <a href="https://www.facebook.com/policy.php" target="_blank" rel="noopener noreferrer">https://www.facebook.com/policy.php</a>.</p>
        <h3>Twitter Plugin</h3>
        <p>Auf dieser Website sind Funktionen des Dienstes Twitter eingebunden. Diese Funktionen werden angeboten durch die Twitter International Company, One
            Cumberland Place, Fenian Street, Dublin 2, D02 AX07, Irland. Durch das Benutzen von Twitter und der Funktion &bdquo;Re-Tweet&ldquo; werden die von Ihnen
            besuchten Websites mit Ihrem Twitter-Account verkn&uuml;pft und anderen Nutzern bekannt gegeben. Dabei werden auch Daten an Twitter &uuml;bertragen. Wir
            weisen darauf hin, dass wir als Anbieter der Seiten keine Kenntnis vom Inhalt der &uuml;bermittelten Daten sowie deren Nutzung durch Twitter erhalten.
            Weitere Informationen hierzu finden Sie in der Datenschutzerkl&auml;rung von Twitter unter: <a href="https://twitter.com/de/privacy" target="_blank"
                                                                                                           rel="noopener noreferrer">https://twitter.com/de/privacy</a>.
        </p>
        <p>Die Verwendung des Twitter-Plugins erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an einer m&ouml;glichst
            umfangreichen Sichtbarkeit in den Sozialen Medien. Sofern eine entsprechende Einwilligung abgefragt wurde, erfolgt die Verarbeitung ausschlie&szlig;lich
            auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit widerrufbar.</p>
        <p>Die Daten&uuml;bertragung in die USA wird auf die Standardvertragsklauseln der EU-Kommission gest&uuml;tzt. Details finden Sie hier: <a
                href="https://gdpr.twitter.com/en/controller-to-controller-transfers.html" target="_blank" rel="noopener noreferrer">https://gdpr.twitter.com/en/controller-to-controller-transfers.html</a>.
        </p>
        <p>Ihre Datenschutzeinstellungen bei Twitter k&ouml;nnen Sie in den Konto-Einstellungen unter <a href="https://twitter.com/account/settings" target="_blank"
                                                                                                         rel="noopener noreferrer">https://twitter.com/account/settings</a>
            &auml;ndern.</p>
        <h3>Instagram Plugin</h3>
        <p>Auf dieser Website sind Funktionen des Dienstes Instagram eingebunden. Diese Funktionen werden angeboten durch die Facebook Ireland Limited, 4 Grand Canal
            Square, Grand Canal Harbour, Dublin 2, Irland integriert.</p>
        <p>Wenn Sie in Ihrem Instagram-Account eingeloggt sind, k&ouml;nnen Sie durch Anklicken des Instagram-Buttons die Inhalte dieser Website mit Ihrem
            Instagram-Profil verlinken. Dadurch kann Instagram den Besuch dieser Website Ihrem Benutzerkonto zuordnen. Wir weisen darauf hin, dass wir als Anbieter
            der Seiten keine Kenntnis vom Inhalt der &uuml;bermittelten Daten sowie deren Nutzung durch Instagram erhalten.</p>
        <p>Die Speicherung und Analyse der Daten erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an einer m&ouml;glichst
            umfangreichen Sichtbarkeit in den Sozialen Medien. Sofern eine entsprechende Einwilligung abgefragt wurde, erfolgt die Verarbeitung ausschlie&szlig;lich
            auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit widerrufbar.</p>
        <p>Soweit mit Hilfe des hier beschriebenen Tools personenbezogene Daten auf unserer Website erfasst und an Facebook bzw. Instagram weitergeleitet werden,
            sind wir und die Facebook Ireland Limited, 4 Grand Canal Square, Grand Canal Harbour, Dublin 2, Irland gemeinsam f&uuml;r diese Datenverarbeitung
            verantwortlich (Art. 26 DSGVO). Die gemeinsame Verantwortlichkeit beschr&auml;nkt sich dabei ausschlie&szlig;lich auf die Erfassung der Daten und deren
            Weitergabe an Facebook bzw. Instagram. Die nach der Weiterleitung erfolgende Verarbeitung durch Facebook bzw. Instagram ist nicht Teil der gemeinsamen
            Verantwortung. Die uns gemeinsam obliegenden Verpflichtungen wurden in einer Vereinbarung &uuml;ber gemeinsame Verarbeitung festgehalten. Den Wortlaut
            der Vereinbarung finden Sie unter: <a href="https://www.facebook.com/legal/controller_addendum" target="_blank" rel="noopener noreferrer">https://www.facebook.com/legal/controller_addendum</a>.
            Laut dieser Vereinbarung sind wir f&uuml;r die Erteilung der Datenschutzinformationen beim Einsatz des Facebook- bzw. Instagram-Tools und f&uuml;r die
            datenschutzrechtlich sichere Implementierung des Tools auf unserer Website verantwortlich. F&uuml;r die Datensicherheit der Facebook bzw.
            Instagram-Produkte ist Facebook verantwortlich. Betroffenenrechte (z.B. Auskunftsersuchen) hinsichtlich der bei Facebook bzw. Instagram verarbeiteten
            Daten k&ouml;nnen Sie direkt bei Facebook geltend machen. Wenn Sie die Betroffenenrechte bei uns geltend machen, sind wir verpflichtet, diese an Facebook
            weiterzuleiten.</p>
        <p>Die Daten&uuml;bertragung in die USA wird auf die Standardvertragsklauseln der EU-Kommission gest&uuml;tzt. Details finden Sie hier: <a
                href="https://www.facebook.com/legal/EU_data_transfer_addendum" target="_blank" rel="noopener noreferrer">https://www.facebook.com/legal/EU_data_transfer_addendum</a>,
            <a href="https://help.instagram.com/519522125107875" target="_blank" rel="noopener noreferrer">https://help.instagram.com/519522125107875</a> und <a
                href="https://de-de.facebook.com/help/566994660333381" target="_blank"
                rel="noopener noreferrer">https://de-de.facebook.com/help/566994660333381</a>.</p>
        <p>Weitere Informationen hierzu finden Sie in der Datenschutzerkl&auml;rung von Instagram: <a href="https://instagram.com/about/legal/privacy/"
                                                                                                      target="_blank" rel="noopener noreferrer">https://instagram.com/about/legal/privacy/</a>.
        </p>
        <h3>LinkedIn Plugin</h3>
        <p>Diese Website nutzt Funktionen des Netzwerks LinkedIn. Anbieter ist die LinkedIn Ireland Unlimited Company, Wilton Plaza, Wilton Place, Dublin 2,
            Irland.</p>
        <p>Bei jedem Abruf einer Seite dieser Website, die Funktionen von LinkedIn enth&auml;lt, wird eine Verbindung zu Servern von LinkedIn aufgebaut. LinkedIn
            wird dar&uuml;ber informiert, dass Sie diese Website mit Ihrer IP-Adresse besucht haben. Wenn Sie den &bdquo;Recommend-Button&ldquo; von LinkedIn
            anklicken und in Ihrem Account bei LinkedIn eingeloggt sind, ist es LinkedIn m&ouml;glich, Ihren Besuch auf dieser Website Ihnen und Ihrem Benutzerkonto
            zuzuordnen. Wir weisen darauf hin, dass wir als Anbieter der Seiten keine Kenntnis vom Inhalt der &uuml;bermittelten Daten sowie deren Nutzung durch
            LinkedIn haben.</p>
        <p>Die Verwendung des LinkedIn-Plugins erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an einer m&ouml;glichst
            umfangreichen Sichtbarkeit in den Sozialen Medien. Sofern eine entsprechende Einwilligung abgefragt wurde, erfolgt die Verarbeitung ausschlie&szlig;lich
            auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit widerrufbar.</p>
        <p>Die Daten&uuml;bertragung in die USA wird auf die Standardvertragsklauseln der EU-Kommission gest&uuml;tzt. Details finden Sie hier: <a
                href="https://www.linkedin.com/help/linkedin/answer/62538/datenubertragung-aus-der-eu-dem-ewr-und-der-schweiz?lang=de" target="_blank"
                rel="noopener noreferrer">https://www.linkedin.com/help/linkedin/answer/62538/datenubertragung-aus-der-eu-dem-ewr-und-der-schweiz?lang=de</a></p>
        <p>Weitere Informationen hierzu finden Sie in der Datenschutzerkl&auml;rung von LinkedIn unter: <a href="https://www.linkedin.com/legal/privacy-policy"
                                                                                                           target="_blank" rel="noopener noreferrer">https://www.linkedin.com/legal/privacy-policy</a>.
        </p>
        <h3>XING Plugin</h3>
        <p>Diese Website nutzt Funktionen des Netzwerks XING. Anbieter ist die New Work SE, Dammtorstra&szlig;e 30, 20354 Hamburg, Deutschland.</p>
        <p>Bei jedem Abruf einer unserer Seiten, die Funktionen von XING enth&auml;lt, wird eine Verbindung zu Servern von XING hergestellt. Eine Speicherung von
            personenbezogenen Daten erfolgt dabei nach unserer Kenntnis nicht. Insbesondere werden keine IP-Adressen gespeichert oder das Nutzungsverhalten
            ausgewertet.</p>
        <p>Die Speicherung und Analyse der Daten erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an einer m&ouml;glichst
            umfangreichen Sichtbarkeit in den Sozialen Medien. Sofern eine entsprechende Einwilligung abgefragt wurde, erfolgt die Verarbeitung ausschlie&szlig;lich
            auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit widerrufbar.</p>
        <p>Weitere Information zum Datenschutz und dem XING Share-Button finden Sie in der Datenschutzerkl&auml;rung von XING unter: <a
                href="https://www.xing.com/app/share?op=data_protection" target="_blank" rel="noopener noreferrer">https://www.xing.com/app/share?op=data_protection</a>.
        </p>
        <h3>Pinterest Plugin</h3>
        <p>Auf dieser Website verwenden wir Social Plugins des sozialen Netzwerkes Pinterest, das von der Pinterest Europe Ltd., Palmerston House, 2nd Floor, Fenian
            Street, Dublin 2, Irland betrieben wird.</p>
        <p>Wenn Sie eine Seite aufrufen, die ein solches Plugin enth&auml;lt, stellt Ihr Browser eine direkte Verbindung zu den Servern von Pinterest her. Das Plugin
            &uuml;bermittelt dabei Protokolldaten an den Server von Pinterest in die USA. Diese Protokolldaten enthalten m&ouml;glicherweise Ihre IP-Adresse, die
            Adresse der besuchten Websites, die ebenfalls Pinterest-Funktionen enthalten, Art und Einstellungen des Browsers, Datum und Zeitpunkt der Anfrage, Ihre
            Verwendungsweise von Pinterest sowie Cookies.</p>
        <p>Die Speicherung und Analyse der Daten erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an einer m&ouml;glichst
            umfangreichen Sichtbarkeit in den Sozialen Medien. Sofern eine entsprechende Einwilligung abgefragt wurde, erfolgt die Verarbeitung ausschlie&szlig;lich
            auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit widerrufbar.</p>
        <p>Weitere Informationen zu Zweck, Umfang und weiterer Verarbeitung und Nutzung der Daten durch Pinterest sowie Ihre diesbez&uuml;glichen Rechte und M&ouml;glichkeiten
            zum Schutz Ihrer Privatsph&auml;re finden Sie in den Datenschutzhinweisen von Pinterest: <a href="https://policy.pinterest.com/de/privacy-policy"
                                                                                                        target="_blank" rel="noopener noreferrer">https://policy.pinterest.com/de/privacy-policy</a>.
        </p>
        <h2>6. Analyse-Tools und Werbung</h2>
        <h3>Google Tag Manager</h3>
        <p>Wir setzen den Google Tag Manager ein. Anbieter ist die Google Ireland Limited, Gordon House, Barrow Street, Dublin 4, Irland.</p>
        <p>Der Google Tag Manager ist ein Tool, mit dessen Hilfe wir Tracking- oder Statistik-Tools und andere Technologien auf unserer Website einbinden k&ouml;nnen.
            Der Google Tag Manager selbst erstellt keine Nutzerprofile, speichert keine Cookies und nimmt keine eigenst&auml;ndigen Analysen vor. Er dient lediglich
            der Verwaltung und Ausspielung der &uuml;ber ihn eingebundenen Tools. Der Google Tag Manager erfasst jedoch Ihre IP-Adresse, die auch an das
            Mutterunternehmen von Google in die Vereinigten Staaten &uuml;bertragen werden kann.</p>
        <p>Der Einsatz des Google Tag Managers erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an einer
            schnellen und unkomplizierten Einbindung und Verwaltung verschiedener Tools auf seiner Website. Sofern eine entsprechende Einwilligung abgefragt wurde,
            erfolgt die Verarbeitung ausschlie&szlig;lich auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit widerrufbar.</p>
        <h3>Google Analytics</h3>
        <p>Diese Website nutzt Funktionen des Webanalysedienstes Google Analytics. Anbieter ist die Google Ireland Limited (&bdquo;Google&ldquo;), Gordon House,
            Barrow Street, Dublin 4, Irland.</p>
        <p>Google Analytics erm&ouml;glicht es dem Websitebetreiber, das Verhalten der Websitebesucher zu analysieren. Hierbei erh&auml;lt der Websitebetreiber
            verschiedene Nutzungsdaten, wie z.B. Seitenaufrufe, Verweildauer, verwendete Betriebssysteme und Herkunft des Nutzers. Diese Daten werden von Google ggf.
            in einem Profil zusammengefasst, das dem jeweiligen Nutzer bzw. dessen Endger&auml;t zugeordnet ist.</p>
        <p>Google Analytics verwendet Technologien, die die Wiedererkennung des Nutzers zum Zwecke der Analyse des Nutzerverhaltens erm&ouml;glichen (z.B. Cookies
            oder Device-Fingerprinting). Die von Google erfassten Informationen &uuml;ber die Benutzung dieser Website werden in der Regel an einen Server von Google
            in den USA &uuml;bertragen und dort gespeichert.</p>
        <p>Die Nutzung dieses Analyse-Tools erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an der Analyse
            des Nutzerverhaltens, um sowohl sein Webangebot als auch seine Werbung zu optimieren. Sofern eine entsprechende Einwilligung abgefragt wurde (z. B. eine
            Einwilligung zur Speicherung von Cookies), erfolgt die Verarbeitung ausschlie&szlig;lich auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung
            ist jederzeit widerrufbar.</p>
        <p>Die Daten&uuml;bertragung in die USA wird auf die Standardvertragsklauseln der EU-Kommission gest&uuml;tzt. Details finden Sie hier: <a
                href="https://privacy.google.com/businesses/controllerterms/mccs/" target="_blank" rel="noopener noreferrer">https://privacy.google.com/businesses/controllerterms/mccs/</a>.
        </p> <h4>IP Anonymisierung</h4>
        <p>Wir haben auf dieser Website die Funktion IP-Anonymisierung aktiviert. Dadurch wird Ihre IP-Adresse von Google innerhalb von Mitgliedstaaten der Europ&auml;ischen
            Union oder in anderen Vertragsstaaten des Abkommens &uuml;ber den Europ&auml;ischen Wirtschaftsraum vor der &Uuml;bermittlung in die USA gek&uuml;rzt.
            Nur in Ausnahmef&auml;llen wird die volle IP-Adresse an einen Server von Google in den USA &uuml;bertragen und dort gek&uuml;rzt. Im Auftrag des
            Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports &uuml;ber die Websiteaktivit&auml;ten
            zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegen&uuml;ber dem Websitebetreiber zu
            erbringen. Die im Rahmen von Google Analytics von Ihrem Browser &uuml;bermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengef&uuml;hrt.</p>
        <h4>Browser Plugin</h4>
        <p>Sie k&ouml;nnen die Erfassung und Verarbeitung Ihrer Daten durch Google verhindern, indem Sie das unter dem folgenden Link verf&uuml;gbare Browser-Plugin
            herunterladen und installieren: <a href="https://tools.google.com/dlpage/gaoptout?hl=de" target="_blank" rel="noopener noreferrer">https://tools.google.com/dlpage/gaoptout?hl=de</a>.
        </p>
        <p>Mehr Informationen zum Umgang mit Nutzerdaten bei Google Analytics finden Sie in der Datenschutzerkl&auml;rung von Google: <a
                href="https://support.google.com/analytics/answer/6004245?hl=de" target="_blank" rel="noopener noreferrer">https://support.google.com/analytics/answer/6004245?hl=de</a>.
        </p><h4>Auftragsverarbeitung</h4>
        <p>Wir haben mit Google einen Vertrag zur Auftragsverarbeitung abgeschlossen und setzen die strengen Vorgaben der deutschen Datenschutzbeh&ouml;rden bei der
            Nutzung von Google Analytics vollst&auml;ndig um.</p>
        <h4>Demografische Merkmale bei Google Analytics</h4>
        <p>Diese Website nutzt die Funktion &bdquo;demografische Merkmale&ldquo; von Google Analytics, um den Websitebesuchern passende Werbeanzeigen innerhalb des
            Google-Werbenetzwerks anzeigen zu k&ouml;nnen. Dadurch k&ouml;nnen Berichte erstellt werden, die Aussagen zu Alter, Geschlecht und Interessen der
            Seitenbesucher enthalten. Diese Daten stammen aus interessenbezogener Werbung von Google sowie aus Besucherdaten von Drittanbietern. Diese Daten k&ouml;nnen
            keiner bestimmten Person zugeordnet werden. Sie k&ouml;nnen diese Funktion jederzeit &uuml;ber die Anzeigeneinstellungen in Ihrem Google-Konto
            deaktivieren oder die Erfassung Ihrer Daten durch Google Analytics wie im Punkt &bdquo;Widerspruch gegen Datenerfassung&ldquo; dargestellt generell
            untersagen.</p>
        <h4>Google Analytics E-Commerce-Tracking</h4>
        <p>Diese Website nutzt die Funktion &bdquo;E-Commerce-Tracking&ldquo; von Google Analytics. Mit Hilfe von E-Commerce-Tracking kann der Websitebetreiber das
            Kaufverhalten der Websitebesucher zur Verbesserung seiner Online-Marketing-Kampagnen analysieren. Hierbei werden Informationen, wie zum Beispiel die get&auml;tigten
            Bestellungen, durchschnittliche Bestellwerte, Versandkosten und die Zeit von der Ansicht bis zum Kauf eines Produktes erfasst. Diese Daten k&ouml;nnen
            von Google unter einer Transaktions-ID zusammengefasst werden, die dem jeweiligen Nutzer bzw. dessen Ger&auml;t zugeordnet ist.</p>
        <h4>Speicherdauer</h4>
        <p>Bei Google gespeicherte Daten auf Nutzer- und Ereignisebene, die mit Cookies, Nutzerkennungen (z.&nbsp;B. User ID) oder Werbe-IDs (z.&nbsp;B.
            DoubleClick-Cookies, Android-Werbe-ID) verkn&uuml;pft sind, werden nach 14 Monaten anonymisiert bzw. gel&ouml;scht. Details hierzu ersehen Sie unter
            folgendem Link: <a href="https://support.google.com/analytics/answer/7667196?hl=de" target="_blank" rel="noopener noreferrer">https://support.google.com/analytics/answer/7667196?hl=de</a>
        </p>

        <h3>IONOS WebAnalytics</h3>
        <p>Diese Website nutzt die Analysedienste von IONOS WebAnalytics (im Folgenden: IONOS). Anbieter ist die 1&amp;1 IONOS SE, Elgendorfer Stra&szlig;e 57, D
            &ndash; 56410 Montabaur. Im Rahmen der Analysen mit IONOS k&ouml;nnen u. a. Besucherzahlen und &ndash;verhalten (z.&nbsp;B. Anzahl der Seitenaufrufe,
            Dauer eines Webseitenbesuchs, Absprungraten), Besucherquellen (d. h., von welcher Seite der Besucher kommt), Besucherstandorte sowie technische Daten
            (Browser- und Betriebssystemversionen) analysiert werden. Zu diesem Zweck speichert IONOS insbesondere folgende Daten:</p>
        <ul>
            <li>Referrer (zuvor besuchte Webseite)</li>
            <li>angeforderte Webseite oder Datei</li>
            <li>Browsertyp und Browserversion</li>
            <li>verwendetes Betriebssystem</li>
            <li>verwendeter Ger&auml;tetyp</li>
            <li>Uhrzeit des Zugriffs</li>
            <li>IP-Adresse in anonymisierter Form (wird nur zur Feststellung des Orts des Zugriffs verwendet)</li>
        </ul>
        <p>Die Datenerfassung erfolgt laut IONOS vollst&auml;ndig anonymisiert, sodass sie nicht zu einzelnen Personen zur&uuml;ckverfolgt werden kann. Cookies
            werden von IONOS WebAnalytics nicht gespeichert.</p>
        <p>Die Speicherung und Analyse der Daten erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an der
            statistischen Analyse des Nutzerverhaltens, um sowohl sein Webangebot als auch seine Werbung zu optimieren. Sofern eine entsprechende Einwilligung
            abgefragt wurde, erfolgt die Verarbeitung ausschlie&szlig;lich auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit
            widerrufbar.</p>
        <p>Weitere Informationen zur Datenerfassung und Verarbeitung durch IONOS WebAnalytics entnehmen Sie der Datenschutzerklaerung von IONOS unter folgendem
            Link:</p>
        <p><a href="https://www.ionos.de/terms-gtc/index.php?id=6" target="_blank" rel="noopener noreferrer">https://www.ionos.de/terms-gtc/index.php?id=6</a></p>
        <h4>Auftragsverarbeitung</h4>
        <p>Wir haben mit IONOS einen Vertrag zur Auftragsverarbeitung abgeschlossen. Dieser Vertrag soll den datenschutzkonformen Umgang mit Ihren personenbezogenen
            Daten durch IONOS sicherstellen.</p>
        <h3>WP Statistics</h3>
        <p>Diese Website nutzt das Analysetool WP Statistics, um Besucherzugriffe statistisch auszuwerten. Anbieter ist Veronalabs, ARENCO Tower, 27th Floor, Dubai
            Media City, Dubai, Dubai 23816, UAE (<a href="https://veronalabs.com" target="_blank" rel="noopener noreferrer">https://veronalabs.com</a>).</p>
        <p>Mit WP Statistics k&ouml;nnen wir die Nutzung unserer Website analysieren. WP Statistics erfasst dabei u. a. Logdateien (IP-Adresse, Referrer, verwendete
            Browser, Herkunft des Nutzers, verwendete Suchmaschine) und Aktionen, die die Websitebesucher auf der Seite get&auml;tigt haben (z. B. Klicks und
            Ansichten).</p>
        <p>Die mit WP Statistics erfassten Daten werden ausschlie&szlig;lich auf unserem eigenen Server gespeichert und nicht an WordPress weitergegeben.</p>
        <p>Die Nutzung dieses Analyse-Tools erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Wir haben ein berechtigtes Interesse an der anonymisierten Analyse
            des Nutzerverhaltens, um sowohl unser Webangebot als auch unsere Werbung zu optimieren. Sofern eine entsprechende Einwilligung abgefragt wurde (z. B.
            eine Einwilligung zur Speicherung von Cookies), erfolgt die Verarbeitung ausschlie&szlig;lich auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die
            Einwilligung ist jederzeit widerrufbar.</p>
        <h3>Google Ads</h3>
        <p>Der Websitebetreiber verwendet Google Ads. Google Ads ist ein Online-Werbeprogramm der Google Ireland Limited (&bdquo;Google&ldquo;), Gordon House, Barrow
            Street, Dublin 4, Irland.</p>
        <p>Google Ads erm&ouml;glicht es uns Werbeanzeigen in der Google-Suchmaschine oder auf Drittwebseiten auszuspielen, wenn der Nutzer bestimmte Suchbegriffe
            bei Google eingibt (Keyword-Targeting). Ferner k&ouml;nnen zielgerichtete Werbeanzeigen anhand der bei Google vorhandenen Nutzerdaten (z.B. Standortdaten
            und Interessen) ausgespielt werden (Zielgruppen-Targeting). Wir als Websitebetreiber k&ouml;nnen diese Daten quantitativ auswerten, indem wir
            beispielsweise analysieren, welche Suchbegriffe zur Ausspielung unserer Werbeanzeigen gef&uuml;hrt haben und wie viele Anzeigen zu entsprechenden Klicks
            gef&uuml;hrt haben.</p>
        <p>Die Nutzung von Google Ads erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an einer m&ouml;glichst
            effektiven Vermarktung seiner Dienstleistung Produkte.</p>
        <p>Die Daten&uuml;bertragung in die USA wird auf die Standardvertragsklauseln der EU-Kommission gest&uuml;tzt. Details finden Sie hier: <a
                href="https://policies.google.com/privacy/frameworks" target="_blank"
                rel="noopener noreferrer">https://policies.google.com/privacy/frameworks</a> und <a
                href="https://privacy.google.com/businesses/controllerterms/mccs/" target="_blank" rel="noopener noreferrer">https://privacy.google.com/businesses/controllerterms/mccs/</a>.
        </p>
        <h3>Google AdSense (nicht personalisiert)</h3>
        <p>Diese Website nutzt Google AdSense, einen Dienst zum Einbinden von Werbeanzeigen. Anbieter ist die Google Ireland Limited (&bdquo;Google&ldquo;), Gordon
            House, Barrow Street, Dublin 4, Irland.</p>
        <p>Wir nutzen Google AdSense im &bdquo;nicht-personalisierten&ldquo; Modus. Im Gegensatz zum personalisierten Modus beruhen die Werbeanzeigen daher nicht auf
            Ihrem fr&uuml;heren Nutzerverhalten und es wird kein Nutzerprofil von Ihnen erstellt. Stattdessen werden bei der Auswahl der Werbung sogenannte &bdquo;Kontextinformationen&ldquo;
            herangezogen. Die ausgew&auml;hlten Werbeanzeigen richten sich dann beispielsweise nach Ihrem Standort, dem Inhalt der Website, auf der Sie sich befinden
            oder nach Ihren aktuellen Suchbegriffen. Mehr zu den Unterschieden zwischen personalisiertem und nicht-personalisiertem Targeting mit Google AdSense
            finden Sie unter: <a href="https://support.google.com/adsense/answer/9007336" target="_blank" rel="noopener noreferrer">https://support.google.com/adsense/answer/9007336</a>.
        </p>
        <p>Bitte beachten Sie, dass auch beim Einsatz von Google Adsense im nicht-personalisierten Modus Cookies oder vergleichbare Wiedererkennungstechnologien
            (z.B. Device-Fingerprinting) verwendet werden k&ouml;nnen. Diese werden laut Google zur Bek&auml;mpfung von Betrug und Missbrauch eingesetzt.</p>
        <p>Die Nutzung von AdSense erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an einer m&ouml;glichst
            effektiven Vermarktung seiner Website. Sofern eine entsprechende Einwilligung abgefragt wurde, erfolgt die Verarbeitung ausschlie&szlig;lich auf
            Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit widerrufbar.</p>
        <p>Die Daten&uuml;bertragung in die USA wird auf die Standardvertragsklauseln der EU-Kommission gest&uuml;tzt. Details finden Sie hier: <a
                href="https://privacy.google.com/businesses/controllerterms/mccs/" target="_blank" rel="noopener noreferrer">https://privacy.google.com/businesses/controllerterms/mccs/</a>.
        </p>
        <p>Sie k&ouml;nnen Ihre Werbeeinstellungen selbstst&auml;ndig in Ihrem Nutzer-Account anpassen. Klicken Sie hierzu auf folgenden Link und loggen Sie sich
            ein: <a href="https://adssettings.google.com/authenticated" target="_blank" rel="noopener noreferrer">https://adssettings.google.com/authenticated</a>.
        </p>
        <p>Weitere Informationen zu den Werbetechnologien von Google finden Sie hier: <a href="https://policies.google.com/technologies/ads" target="_blank"
                                                                                         rel="noopener noreferrer">https://policies.google.com/technologies/ads</a>
            und <a href="https://www.google.de/intl/de/policies/privacy/" target="_blank"
                   rel="noopener noreferrer">https://www.google.de/intl/de/policies/privacy/</a>.</p>
        <h3>Google Remarketing</h3>
        <p>Diese Website nutzt die Funktionen von Google Analytics Remarketing. Anbieter ist die Google Ireland Limited (&bdquo;Google&ldquo;), Gordon House, Barrow
            Street, Dublin 4, Irland.</p>
        <p>Google Remarketing analysiert Ihr Nutzerverhalten auf unserer Website (z.B. Klick auf bestimmte Produkte), um Sie in bestimmte Werbe-Zielgruppen
            einzuordnen und Ihnen anschlie&szlig;end beim Besuch von anderen Onlineangeboten passende Webebotschaften auszuspielen (Remarketing bzw.
            Retargeting).</p>
        <p>Des Weiteren k&ouml;nnen die mit Google Remarketing erstellten Werbe-Zielgruppen mit den ger&auml;te&uuml;bergreifenden Funktionen von Google verkn&uuml;pft
            werden. Auf diese Weise k&ouml;nnen interessenbezogene, personalisierte Werbebotschaften, die in Abh&auml;ngigkeit Ihres fr&uuml;heren Nutzungs- und
            Surfverhaltens auf einem Endger&auml;t (z. B. Handy) an Sie angepasst wurden auch auf einem anderen Ihrer Endger&auml;te (z. B. Tablet oder PC) angezeigt
            werden.</p>
        <p>Wenn Sie &uuml;ber einen Google-Account verf&uuml;gen, k&ouml;nnen Sie der personalisierten Werbung unter folgendem Link widersprechen: <a
                href="https://www.google.com/settings/ads/onweb/" target="_blank" rel="noopener noreferrer">https://www.google.com/settings/ads/onweb/</a>.</p>
        <p>Die Nutzung von Google Remarketing erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an einer m&ouml;glichst
            effektiven Vermarktung seiner Produkte. Sofern eine entsprechende Einwilligung abgefragt wurde, erfolgt die Verarbeitung ausschlie&szlig;lich auf
            Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit widerrufbar.</p>
        <p>Weitergehende Informationen und die Datenschutzbestimmungen finden Sie in der Datenschutzerkl&auml;rung von Google unter: <a
                href="https://policies.google.com/technologies/ads?hl=de" target="_blank" rel="noopener noreferrer">https://policies.google.com/technologies/ads?hl=de</a>.
        </p>
        <h4>Zielgruppenbildung mit Kundenabgleich</h4>
        <p>Zur Zielgruppenbildung verwenden wir unter anderem den Kundenabgleich von Google Remarketing. Hierbei &uuml;bergeben wir bestimmte Kundendaten (z.B.
            E-Mail-Adressen) aus unseren Kundenlisten an Google. Sind die betreffenden Kunden Google-Nutzer und in ihrem Google-Konto eingeloggt, werden ihnen
            passende Werbebotschaften innerhalb des Google-Netzwerks (z.B. bei YouTube, Gmail oder in der Suchmaschine) angezeigt.</p>
        <h3>Facebook Pixel</h3>
        <p>Diese Website nutzt zur Konversionsmessung der Besucheraktions-Pixel von Facebook. Anbieter dieses Dienstes ist die Facebook Ireland Limited, 4 Grand
            Canal Square, Dublin 2, Irland. Die erfassten Daten werden nach Aussage von Facebook jedoch auch in die USA und in andere Drittl&auml;nder &uuml;bertragen.</p>
        <p>So kann das Verhalten der Seitenbesucher nachverfolgt werden, nachdem diese durch Klick auf eine Facebook-Werbeanzeige auf die Website des Anbieters
            weitergeleitet wurden. Dadurch k&ouml;nnen die Wirksamkeit der Facebook-Werbeanzeigen f&uuml;r statistische und Marktforschungszwecke ausgewertet werden
            und zuk&uuml;nftige Werbema&szlig;nahmen optimiert werden.</p>
        <p>Die erhobenen Daten sind f&uuml;r uns als Betreiber dieser Website anonym, wir k&ouml;nnen keine R&uuml;ckschl&uuml;sse auf die Identit&auml;t der Nutzer
            ziehen. Die Daten werden aber von Facebook gespeichert und verarbeitet, sodass eine Verbindung zum jeweiligen Nutzerprofil m&ouml;glich ist und Facebook
            die Daten f&uuml;r eigene Werbezwecke, entsprechend der <a href="https://de-de.facebook.com/about/privacy/" target="_blank" rel="noopener noreferrer">Facebook-Datenverwendungsrichtlinie</a>
            verwenden kann. Dadurch kann Facebook das Schalten von Werbeanzeigen auf Seiten von Facebook sowie au&szlig;erhalb von Facebook erm&ouml;glichen. Diese
            Verwendung der Daten kann von uns als Seitenbetreiber nicht beeinflusst werden.</p>
        <p>Die Nutzung von Facebook-Pixel erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an effektiven
            Werbema&szlig;nahmen unter Einschluss der sozialen Medien. Sofern eine entsprechende Einwilligung abgefragt wurde (z. B. eine Einwilligung zur
            Speicherung von Cookies), erfolgt die Verarbeitung ausschlie&szlig;lich auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit
            widerrufbar.</p>
        <p>Die Daten&uuml;bertragung in die USA wird auf die Standardvertragsklauseln der EU-Kommission gest&uuml;tzt. Details finden Sie hier: <a
                href="https://www.facebook.com/legal/EU_data_transfer_addendum" target="_blank" rel="noopener noreferrer">https://www.facebook.com/legal/EU_data_transfer_addendum</a>
            und <a href="https://de-de.facebook.com/help/566994660333381" target="_blank"
                   rel="noopener noreferrer">https://de-de.facebook.com/help/566994660333381</a>.</p>
        <p>Soweit mit Hilfe des hier beschriebenen Tools personenbezogene Daten auf unserer Website erfasst und an Facebook weitergeleitet werden, sind wir und die
            Facebook Ireland Limited, 4 Grand Canal Square, Grand Canal Harbour, Dublin 2, Irland gemeinsam f&uuml;r diese Datenverarbeitung verantwortlich (Art. 26
            DSGVO). Die gemeinsame Verantwortlichkeit beschr&auml;nkt sich dabei ausschlie&szlig;lich auf die Erfassung der Daten und deren Weitergabe an Facebook.
            Die nach der Weiterleitung erfolgende Verarbeitung durch Facebook ist nicht Teil der gemeinsamen Verantwortung. Die uns gemeinsam obliegenden
            Verpflichtungen wurden in einer Vereinbarung &uuml;ber gemeinsame Verarbeitung festgehalten. Den Wortlaut der Vereinbarung finden Sie unter: <a
                href="https://www.facebook.com/legal/controller_addendum" target="_blank" rel="noopener noreferrer">https://www.facebook.com/legal/controller_addendum</a>.
            Laut dieser Vereinbarung sind wir f&uuml;r die Erteilung der Datenschutzinformationen beim Einsatz des Facebook-Tools und f&uuml;r die
            datenschutzrechtlich sichere Implementierung des Tools auf unserer Website verantwortlich. F&uuml;r die Datensicherheit der Facebook-Produkte ist
            Facebook verantwortlich. Betroffenenrechte (z.B. Auskunftsersuchen) hinsichtlich der bei Facebook verarbeiteten Daten k&ouml;nnen Sie direkt bei Facebook
            geltend machen. Wenn Sie die Betroffenenrechte bei uns geltend machen, sind wir verpflichtet, diese an Facebook weiterzuleiten.</p>
        <p>In den Datenschutzhinweisen von Facebook finden Sie weitere Hinweise zum Schutz Ihrer Privatsph&auml;re: <a
                href="https://de-de.facebook.com/about/privacy/" target="_blank" rel="noopener noreferrer">https://de-de.facebook.com/about/privacy/</a>.</p>
        <p>Sie k&ouml;nnen au&szlig;erdem die Remarketing-Funktion &bdquo;Custom Audiences&ldquo; im Bereich Einstellungen f&uuml;r Werbeanzeigen unter <a
                href="https://www.facebook.com/ads/preferences/?entry_product=ad_settings_screen" target="_blank" rel="noopener noreferrer">https://www.facebook.com/ads/preferences/?entry_product=ad_settings_screen</a>
            deaktivieren. Dazu m&uuml;ssen Sie bei Facebook angemeldet sein.</p>
        <p>Wenn Sie kein Facebook Konto besitzen, k&ouml;nnen Sie nutzungsbasierte Werbung von Facebook auf der Website der European Interactive Digital Advertising
            Alliance deaktivieren: <a href="http://www.youronlinechoices.com/de/praferenzmanagement/" target="_blank" rel="noopener noreferrer">http://www.youronlinechoices.com/de/praferenzmanagement/</a>.
        </p>
        <h2>7. Plugins und Tools</h2>
        <h3>YouTube mit erweitertem Datenschutz</h3>
        <p>Diese Website bindet Videos der YouTube ein. Betreiber der Seiten ist die Google Ireland Limited (&bdquo;Google&ldquo;), Gordon House, Barrow Street,
            Dublin 4, Irland.</p>
        <p>Wir nutzen YouTube im erweiterten Datenschutzmodus. Dieser Modus bewirkt laut YouTube, dass YouTube keine Informationen &uuml;ber die Besucher auf dieser
            Website speichert, bevor diese sich das Video ansehen. Die Weitergabe von Daten an YouTube-Partner wird durch den erweiterten Datenschutzmodus hingegen
            nicht zwingend ausgeschlossen. So stellt YouTube &ndash; unabh&auml;ngig davon, ob Sie sich ein Video ansehen &ndash; eine Verbindung zum Google
            DoubleClick-Netzwerk her.</p>
        <p>Sobald Sie ein YouTube-Video auf dieser Website starten, wird eine Verbindung zu den Servern von YouTube hergestellt. Dabei wird dem YouTube-Server
            mitgeteilt, welche unserer Seiten Sie besucht haben. Wenn Sie in Ihrem YouTube-Account eingeloggt sind, erm&ouml;glichen Sie YouTube, Ihr Surfverhalten
            direkt Ihrem pers&ouml;nlichen Profil zuzuordnen. Dies k&ouml;nnen Sie verhindern, indem Sie sich aus Ihrem YouTube-Account ausloggen.</p>
        <p>Des Weiteren kann YouTube nach Starten eines Videos verschiedene Cookies auf Ihrem Endger&auml;t speichern oder vergleichbare Wiedererkennungstechnologien
            (z.B. Device-Fingerprinting) einsetzen. Auf diese Weise kann YouTube Informationen &uuml;ber Besucher dieser Website erhalten. Diese Informationen werden
            u. a. verwendet, um Videostatistiken zu erfassen, die Anwenderfreundlichkeit zu verbessern und Betrugsversuchen vorzubeugen.</p>
        <p>Gegebenenfalls k&ouml;nnen nach dem Start eines YouTube-Videos weitere Datenverarbeitungsvorg&auml;nge ausgel&ouml;st werden, auf die wir keinen Einfluss
            haben.</p>
        <p>Die Nutzung von YouTube erfolgt im Interesse einer ansprechenden Darstellung unserer Online-Angebote. Dies stellt ein berechtigtes Interesse im Sinne von
            Art. 6 Abs. 1 lit. f DSGVO dar. Sofern eine entsprechende Einwilligung abgefragt wurde, erfolgt die Verarbeitung ausschlie&szlig;lich auf Grundlage von
            Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit widerrufbar.</p>
        <p>Weitere Informationen &uuml;ber Datenschutz bei YouTube finden Sie in deren Datenschutzerkl&auml;rung unter: <a
                href="https://policies.google.com/privacy?hl=de" target="_blank" rel="noopener noreferrer">https://policies.google.com/privacy?hl=de</a>.</p>
        <h3>Vimeo ohne Tracking (Do-Not-Track)</h3>
        <p>Diese Website nutzt Plugins des Videoportals Vimeo. Anbieter ist die Vimeo Inc., 555 West 18th Street, New York, New York 10011, USA.</p>
        <p>Wenn Sie eine unserer mit Vimeo-Videos ausgestatteten Seiten besuchen, wird eine Verbindung zu den Servern von Vimeo hergestellt. Dabei wird dem
            Vimeo-Server mitgeteilt, welche unserer Seiten Sie besucht haben. Zudem erlangt Vimeo Ihre IP-Adresse. Wir haben Vimeo jedoch so eingestellt, dass Vimeo
            Ihre Nutzeraktivit&auml;ten nicht nachverfolgen und keine Cookies setzen wird.</p>
        <p>Die Nutzung von Vimeo erfolgt im Interesse einer ansprechenden Darstellung unserer Online-Angebote. Dies stellt ein berechtigtes Interesse im Sinne des
            Art. 6 Abs. 1 lit. f DSGVO dar. Sofern eine entsprechende Einwilligung abgefragt wurde, erfolgt die Verarbeitung ausschlie&szlig;lich auf Grundlage von
            Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit widerrufbar.</p>
        <p>Die Daten&uuml;bertragung in die USA wird auf die Standardvertragsklauseln der EU-Kommission sowie nach Aussage von Vimeo auf &bdquo;berechtigte Gesch&auml;ftsinteressen&ldquo;
            gest&uuml;tzt. Details finden Sie hier: <a href="https://vimeo.com/privacy" target="_blank" rel="noopener noreferrer">https://vimeo.com/privacy</a>.</p>
        <p>Weitere Informationen zum Umgang mit Nutzerdaten finden Sie in der Datenschutzerkl&auml;rung von Vimeo unter: <a href="https://vimeo.com/privacy"
                                                                                                                            target="_blank"
                                                                                                                            rel="noopener noreferrer">https://vimeo.com/privacy</a>.
        </p>
        <h3>Adobe Fonts</h3>
        <p>Diese Website nutzt zur einheitlichen Darstellung bestimmter Schriftarten Web Fonts von Adobe. Anbieter ist die Adobe Systems Incorporated, 345 Park
            Avenue, San Jose, CA 95110-2704, USA (Adobe).</p>
        <p>Beim Aufruf dieser Website l&auml;dt Ihr Browser die ben&ouml;tigten Schriftarten direkt von Adobe, um sie Ihrem Endger&auml;t korrekt anzeigen zu k&ouml;nnen.
            Dabei stellt Ihr Browser eine Verbindung zu den Servern von Adobe in den USA her. Hierdurch erlangt Adobe Kenntnis dar&uuml;ber, dass &uuml;ber Ihre
            IP-Adresse diese Website aufgerufen wurde. Bei der Bereitstellung der Schriftarten werden nach Aussage von Adobe keine Cookies gespeichert.</p>
        <p>Die Speicherung und Analyse der Daten erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an der
            einheitlichen Darstellung des Schriftbildes auf seiner Website. Sofern eine entsprechende Einwilligung abgefragt wurde (z. B. eine Einwilligung zur
            Speicherung von Cookies), erfolgt die Verarbeitung ausschlie&szlig;lich auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit
            widerrufbar.</p>
        <p>Die Daten&uuml;bertragung in die USA wird auf die Standardvertragsklauseln der EU-Kommission gest&uuml;tzt. Details finden Sie hier: <a
                href="https://www.adobe.com/de/privacy/eudatatransfers.html" target="_blank" rel="noopener noreferrer">https://www.adobe.com/de/privacy/eudatatransfers.html</a>.
        </p>
        <p>N&auml;here Informationen zu Adobe Fonts erhalten Sie unter: <a href="https://www.adobe.com/de/privacy/policies/adobe-fonts.html" target="_blank"
                                                                           rel="noopener noreferrer">https://www.adobe.com/de/privacy/policies/adobe-fonts.html</a>.
        </p>
        <p>Die Datenschutzerkl&auml;rung von Adobe finden Sie unter: <a href="https://www.adobe.com/de/privacy/policy.html" target="_blank"
                                                                        rel="noopener noreferrer">https://www.adobe.com/de/privacy/policy.html</a></p>
        <h3>Font Awesome (lokales Hosting)</h3>
        <p>Diese Seite nutzt zur einheitlichen Darstellung von Schriftarten Font Awesome. Font Awesome ist lokal installiert. Eine Verbindung zu Servern von
            Fonticons, Inc. findet dabei nicht statt.</p>
        <p>Weitere Informationen zu Font Awesome finden Sie in der Datenschutzerkl&auml;rung f&uuml;r Font Awesome unter: <a href="https://fontawesome.com/privacy"
                                                                                                                             target="_blank"
                                                                                                                             rel="noopener noreferrer">https://fontawesome.com/privacy</a>.
        </p>
        <h3>MyFonts</h3>
        <p>Diese Seite nutzt zur einheitlichen Darstellung von Schriftarten so genannte Web Fonts von MyFonts Inc., 600 Unicorn Park Drive, Woburn, Massachusetts
            01801 USA (nachfolgend: MyFonts). Hierbei handelt es sich um Schriftarten, die beim Aufrufen unserer Website in Ihren Browser geladen werden, um ein
            einheitliches Schriftbild bei der Webseitendarstellung zu gew&auml;hrleisten.</p>
        <p>Die Fonts sind lokal installiert. Eine Verbindung zu Servern von MyFonts findet dabei nicht statt. Der Einsatz der Fonts erfolgt auf Grundlage unseres
            berechtigten Interesses an einer einheitlichen Darstellung unserer Website (Art. 6 Abs. 1 lit. f DSGVO).</p>
        <h3>Google Maps</h3>
        <p>Diese Seite nutzt den Kartendienst Google Maps. Anbieter ist die Google Ireland Limited (&bdquo;Google&ldquo;), Gordon House, Barrow Street, Dublin 4,
            Irland.</p>
        <p>Zur Nutzung der Funktionen von Google Maps ist es notwendig, Ihre IP-Adresse zu speichern. Diese Informationen werden in der Regel an einen Server von
            Google in den USA &uuml;bertragen und dort gespeichert. Der Anbieter dieser Seite hat keinen Einfluss auf diese Daten&uuml;bertragung. Wenn Google Maps
            aktiviert ist, kann Google zum Zwecke der einheitlichen Darstellung der Schriftarten Google Web Fonts verwenden. Beim Aufruf von Google Maps l&auml;dt
            Ihr Browser die ben&ouml;tigten Web Fonts in ihren Browsercache, um Texte und Schriftarten korrekt anzuzeigen.</p>
        <p>Die Nutzung von Google Maps erfolgt im Interesse einer ansprechenden Darstellung unserer Online-Angebote und an einer leichten Auffindbarkeit der von uns
            auf der Website angegebenen Orte. Dies stellt ein berechtigtes Interesse im Sinne von Art. 6 Abs. 1 lit. f DSGVO dar. Sofern eine entsprechende
            Einwilligung abgefragt wurde, erfolgt die Verarbeitung ausschlie&szlig;lich auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit
            widerrufbar.</p>
        <p>Die Daten&uuml;bertragung in die USA wird auf die Standardvertragsklauseln der EU-Kommission gest&uuml;tzt. Details finden Sie hier: <a
                href="https://privacy.google.com/businesses/gdprcontrollerterms/" target="_blank" rel="noopener noreferrer">https://privacy.google.com/businesses/gdprcontrollerterms/</a>
            und <a href="https://privacy.google.com/businesses/gdprcontrollerterms/sccs/" target="_blank" rel="noopener noreferrer">https://privacy.google.com/businesses/gdprcontrollerterms/sccs/</a>.
        </p>
        <p>Mehr Informationen zum Umgang mit Nutzerdaten finden Sie in der Datenschutzerkl&auml;rung von Google: <a href="https://policies.google.com/privacy?hl=de"
                                                                                                                    target="_blank" rel="noopener noreferrer">https://policies.google.com/privacy?hl=de</a>.
        </p>
        <h3>OpenStreetMap</h3>
        <p>Wir nutzen den Kartendienst von OpenStreetMap (OSM). Anbieterin ist die Open-Street-Map Foundation (OSMF), 132 Maney Hill Road, Sutton Coldfield, West
            Midlands, B72 1JU, United Kingdom.</p>
        <p>Wenn Sie eine Website besuchen, auf der OpenStreetMap eingebunden ist, werden u. a. Ihre IP-Adresse und weitere Informationen &uuml;ber Ihr Verhalten auf
            dieser Website an die OSMF weitergeleitet. OpenStreetMap speichert hierzu unter Umst&auml;nden Cookies in Ihrem Browser oder setzt vergleichbare
            Wiedererkennungstechnologien ein.</p>
        <p>Ferner kann Ihr Standort erfasst werden, wenn Sie dies in Ihren Ger&auml;teeinstellungen &ndash; z.&nbsp;B. auf Ihrem Handy &ndash; zugelassen haben. Der
            Anbieter dieser Seite hat keinen Einfluss auf diese Daten&uuml;bertragung. Details entnehmen Sie der Datenschutzerkl&auml;rung von OpenStreetMap unter
            folgendem Link: <a href="https://wiki.osmfoundation.org/wiki/Privacy_Policy" target="_blank" rel="noopener noreferrer">https://wiki.osmfoundation.org/wiki/Privacy_Policy</a>.
        </p>
        <p>Die Nutzung von OpenStreetMap erfolgt im Interesse einer ansprechenden Darstellung unserer Online-Angebote und einer leichten Auffindbarkeit der von uns
            auf der Website angegebenen Orte. Dies stellt ein berechtigtes Interesse im Sinne von Art. 6 Abs. 1 lit. f DSGVO dar. Sofern eine entsprechende
            Einwilligung abgefragt wurde (z. B. eine Einwilligung zur Speicherung von Cookies), erfolgt die Verarbeitung ausschlie&szlig;lich auf Grundlage von Art.
            6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit widerrufbar.</p>
        <h3>Google reCAPTCHA</h3>
        <p>Wir nutzen &bdquo;Google reCAPTCHA&ldquo; (im Folgenden &bdquo;reCAPTCHA&ldquo;) auf dieser Website. Anbieter ist die Google Ireland Limited (&bdquo;Google&ldquo;),
            Gordon House, Barrow Street, Dublin 4, Irland.</p>
        <p>Mit reCAPTCHA soll &uuml;berpr&uuml;ft werden, ob die Dateneingabe auf dieser Website (z.&nbsp;B. in einem Kontaktformular) durch einen Menschen oder
            durch ein automatisiertes Programm erfolgt. Hierzu analysiert reCAPTCHA das Verhalten des Websitebesuchers anhand verschiedener Merkmale. Diese Analyse
            beginnt automatisch, sobald der Websitebesucher die Website betritt. Zur Analyse wertet reCAPTCHA verschiedene Informationen aus (z.&nbsp;B. IP-Adresse,
            Verweildauer des Websitebesuchers auf der Website oder vom Nutzer get&auml;tigte Mausbewegungen). Die bei der Analyse erfassten Daten werden an Google
            weitergeleitet.</p>
        <p>Die reCAPTCHA-Analysen laufen vollst&auml;ndig im Hintergrund. Websitebesucher werden nicht darauf hingewiesen, dass eine Analyse stattfindet.</p>
        <p>Die Speicherung und Analyse der Daten erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse daran,
            seine Webangebote vor missbr&auml;uchlicher automatisierter Aussp&auml;hung und vor SPAM zu sch&uuml;tzen. Sofern eine entsprechende Einwilligung
            abgefragt wurde, erfolgt die Verarbeitung ausschlie&szlig;lich auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit
            widerrufbar.</p>
        <p>Weitere Informationen zu Google reCAPTCHA entnehmen Sie den Google-Datenschutzbestimmungen und den Google Nutzungsbedingungen unter folgenden Links: <a
                href="https://policies.google.com/privacy?hl=de" target="_blank" rel="noopener noreferrer">https://policies.google.com/privacy?hl=de</a> und <a
                href="https://policies.google.com/terms?hl=de" target="_blank" rel="noopener noreferrer">https://policies.google.com/terms?hl=de</a>.</p>
        <h2>8. eCommerce und Zahlungs&shy;anbieter</h2>
        <h3>Verarbeiten von Daten (Kunden- und Vertragsdaten)</h3>
        <p>Wir erheben, verarbeiten und nutzen personenbezogene Daten nur, soweit sie f&uuml;r die Begr&uuml;ndung, inhaltliche Ausgestaltung oder &Auml;nderung des
            Rechtsverh&auml;ltnisses erforderlich sind (Bestandsdaten). Dies erfolgt auf Grundlage von Art. 6 Abs. 1 lit. b DSGVO, der die Verarbeitung von Daten zur
            Erf&uuml;llung eines Vertrags oder vorvertraglicher Ma&szlig;nahmen gestattet. Personenbezogene Daten &uuml;ber die Inanspruchnahme dieser Website
            (Nutzungsdaten) erheben, verarbeiten und nutzen wir nur, soweit dies erforderlich ist, um dem Nutzer die Inanspruchnahme des Dienstes zu erm&ouml;glichen
            oder abzurechnen.</p>
        <p>Die erhobenen Kundendaten werden nach Abschluss des Auftrags oder Beendigung der Gesch&auml;ftsbeziehung gel&ouml;scht. Gesetzliche Aufbewahrungsfristen
            bleiben unber&uuml;hrt.</p>
        <h3>Daten&shy;&uuml;bermittlung bei Vertragsschluss f&uuml;r Online-Shops, H&auml;ndler und Warenversand</h3>
        <p>Wir &uuml;bermitteln personenbezogene Daten an Dritte nur dann, wenn dies im Rahmen der Vertragsabwicklung notwendig ist, etwa an die mit der Lieferung
            der Ware betrauten Unternehmen oder das mit der Zahlungsabwicklung beauftragte Kreditinstitut. Eine weitergehende &Uuml;bermittlung der Daten erfolgt
            nicht bzw. nur dann, wenn Sie der &Uuml;bermittlung ausdr&uuml;cklich zugestimmt haben. Eine Weitergabe Ihrer Daten an Dritte ohne ausdr&uuml;ckliche
            Einwilligung, etwa zu Zwecken der Werbung, erfolgt nicht.</p>
        <p>Grundlage f&uuml;r die Datenverarbeitung ist Art. 6 Abs. 1 lit. b DSGVO, der die Verarbeitung von Daten zur Erf&uuml;llung eines Vertrags oder
            vorvertraglicher Ma&szlig;nahmen gestattet.</p>
        <h3>Zahlungsdienste</h3>
        <p>Wir binden Zahlungsdienste von Drittunternehmen auf unserer Website ein. Wenn Sie einen Kauf bei uns t&auml;tigen, werden Ihre Zahlungsdaten (z.B. Name,
            Zahlungssumme, Kontoverbindung, Kreditkartennummer) vom Zahlungsdienstleister zum Zwecke der Zahlungsabwicklung verarbeitet. F&uuml;r diese Transaktionen
            gelten die jeweiligen Vertrags- und Datenschutzbestimmungen der jeweiligen Anbieter. Der Einsatz der Zahlungsdienstleister erfolgt auf Grundlage von Art.
            6 Abs. 1 lit. b DSGVO (Vertragsabwicklung) sowie im Interesse eines m&ouml;glichst reibungslosen, komfortablen und sicheren Zahlungsvorgangs (Art. 6 Abs.
            1 lit. f DSGVO). Soweit f&uuml;r bestimmte Handlungen Ihre Einwilligung abgefragt wird, ist Art. 6 Abs. 1 lit. a DSGVO Rechtsgrundlage der
            Datenverarbeitung; Einwilligungen sind jederzeit f&uuml;r die Zukunft widerrufbar.</p>
        <p>Folgende Zahlungsdienste / Zahlungsdienstleister setzen wir im Rahmen dieser Website ein:</p>
        <h4>PayPal</h4>
        <p>Anbieter dieses Zahlungsdienstes ist PayPal (Europe) S.&agrave;.r.l. et Cie, S.C.A., 22-24 Boulevard Royal, L-2449 Luxembourg (im Folgenden &bdquo;PayPal&ldquo;).</p>
        <p>Die Daten&uuml;bertragung in die USA wird auf die Standardvertragsklauseln der EU-Kommission gest&uuml;tzt. Details finden Sie hier: <a
                href="https://www.paypal.com/de/webapps/mpp/ua/pocpsa-full" target="_blank" rel="noopener noreferrer">https://www.paypal.com/de/webapps/mpp/ua/pocpsa-full</a>.
        </p>
        <p>Details entnehmen Sie der Datenschutzerkl&auml;rung von PayPal: <a href="https://www.paypal.com/de/webapps/mpp/ua/privacy-full" target="_blank"
                                                                              rel="noopener noreferrer">https://www.paypal.com/de/webapps/mpp/ua/privacy-full</a>.
        </p>
        <h4>Apple Pay</h4>
        <p>Anbieter des Zahlungsdienstes ist Apple Inc., Infinite Loop, Cupertino, CA 95014, USA. Die Datenschutzerkl&auml;rung von Apple finden Sie unter: <a
                href="https://www.apple.com/legal/privacy/de-ww/" target="_blank" rel="noopener noreferrer">https://www.apple.com/legal/privacy/de-ww/</a>.</p>
        <h4>Google Pay</h4>
        <p>Anbieter ist Google Ireland Limited, Gordon House, Barrow Street, Dublin 4, Irland. Die Datenschutzerkl&auml;rung von Google finden Sie hier: <a
                href="https://policies.google.com/privacy" target="_blank" rel="noopener noreferrer">https://policies.google.com/privacy</a>.</p>
        <h4>Klarna</h4>
        <p>Anbieter ist die Klarna AB, Sveav&auml;gen 46, 111 34 Stockholm, Schweden (im Folgenden &bdquo;Klarna&ldquo;). Klarna bietet verschiedene Zahlungsoptionen
            an (z.&nbsp;B. Ratenkauf). Wenn Sie sich f&uuml;r die Bezahlung mit Klarna entscheiden (Klarna-Checkout-L&ouml;sung), wird Klarna verschiedene
            personenbezogene Daten von Ihnen erheben. Klarna nutzt Cookies, um die Verwendung der Klarna-Checkout-L&ouml;sung zu optimieren. Details zum Einsatz von
            Klarna-Cookies entnehmen Sie folgendem Link: <a href="https://cdn.klarna.com/1.0/shared/content/policy/cookie/de_de/checkout.pdf" target="_blank"
                                                            rel="noopener noreferrer">https://cdn.klarna.com/1.0/shared/content/policy/cookie/de_de/checkout.pdf</a>.
        </p>
        <p>Details hierzu k&ouml;nnen Sie in der Datenschutzerkl&auml;rung von Klarna unter folgendem Link nachlesen: <a
                href="https://www.klarna.com/de/datenschutz/" target="_blank" rel="noopener noreferrer">https://www.klarna.com/de/datenschutz/</a>.</p>
        <h4>Paydirekt</h4>
        <p>Anbieter dieses Zahlungsdienstes ist die Paydirekt GmbH, Hamburger Allee 26-28, 60486 Frankfurt am Main, Deutschland (im Folgenden &bdquo;Paydirekt&ldquo;).
            Wenn Sie die Bezahlung mittels Paydirekt ausf&uuml;hren, erhebt Paydirekt verschiedene Transaktionsdaten und leitet diese an die Bank weiter, bei der Sie
            mit Paydirekt registriert sind. Neben den f&uuml;r die Zahlung erforderlichen Daten erhebt Paydirekt im Rahmen der Transaktionsabwicklung ggf. weitere
            Daten wie z.&nbsp;B. Lieferadresse oder einzelne Positionen im Warenkorb. Paydirekt authentifiziert die Transaktion anschlie&szlig;end mit Hilfe des bei
            der Bank hierf&uuml;r hinterlegten Authentifizierungsverfahrens. Anschlie&szlig;end wird der Zahlbetrag von Ihrem Konto auf unser Konto &uuml;berwiesen.
            Weder wir noch Dritte haben Zugriff auf Ihre Kontodaten. Details zur Zahlung mit Paydirekt entnehmen Sie den AGB und den Datenschutzbestimmungen von
            Paydirekt unter: <a href="https://www.paydirekt.de/agb/index.html" target="_blank" rel="noopener noreferrer">https://www.paydirekt.de/agb/index.html</a>.
        </p>
        <h4>Sofort&shy;&uuml;berweisung</h4>
        <p>Anbieter dieses Zahlungsdienstes ist die Sofort GmbH, Theresienh&ouml;he 12, 80339 M&uuml;nchen (im Folgenden &bdquo;Sofort GmbH&ldquo;). Mit Hilfe des
            Verfahrens &bdquo;Sofort&uuml;berweisung&ldquo; erhalten wir in Echtzeit eine Zahlungsbest&auml;tigung von der Sofort GmbH und k&ouml;nnen unverz&uuml;glich
            mit der Erf&uuml;llung unserer Verbindlichkeiten beginnen. Wenn Sie sich f&uuml;r die Zahlungsart &bdquo;Sofort&uuml;berweisung&ldquo; entschieden haben,
            &uuml;bermitteln Sie die PIN und eine g&uuml;ltige TAN an die Sofort GmbH, mit der diese sich in Ihr Online-Banking-Konto einloggen kann. Sofort GmbH
            &uuml;berpr&uuml;ft nach dem Einloggen automatisch Ihren Kontostand und f&uuml;hrt die &Uuml;berweisung an uns mit Hilfe der von Ihnen &uuml;bermittelten
            TAN durch. Anschlie&szlig;end &uuml;bermittelt sie uns unverz&uuml;glich eine Transaktionsbest&auml;tigung. Nach dem Einloggen werden au&szlig;erdem Ihre
            Ums&auml;tze, der Kreditrahmen des Dispokredits und das Vorhandensein anderer Konten sowie deren Best&auml;nde automatisiert gepr&uuml;ft. Neben der PIN
            und der TAN werden auch die von Ihnen eingegebenen Zahlungsdaten sowie Daten zu Ihrer Person an die Sofort GmbH &uuml;bermittelt. Bei den Daten zu Ihrer
            Person handelt es sich um Vor- und Nachnamen, Adresse, Telefonnummer(n), E-Mail-Adresse, IP-Adresse und ggf. weitere zur Zahlungsabwicklung erforderliche
            Daten. Die &Uuml;bermittlung dieser Daten ist notwendig, um Ihre Identit&auml;t zweifelsfrei zu festzustellen und Betrugsversuchen vorzubeugen. Details
            zur Zahlung mit Sofort&uuml;berweisung entnehmen Sie folgenden Links: <a href="https://www.sofort.de/datenschutz.html" target="_blank"
                                                                                     rel="noopener noreferrer">https://www.sofort.de/datenschutz.html</a> und <a
                href="https://www.klarna.com/sofort/" target="_blank" rel="noopener noreferrer">https://www.klarna.com/sofort/</a>.</p>
        <h4>Amazon Pay</h4>
        <p>Anbieter dieses Zahlungsdienstes ist die Amazon Payments Europe S.C.A., 38 avenue J.F. Kennedy, L-1855 Luxemburg.</p>
        <p>Details zum Umgang mit Ihren Daten k&ouml;nnen Sie in der Datenschutzerkl&auml;rung von Amazon Pay unter folgendem Link nachlesen: <a
                href="https://pay.amazon.de/help/201212490?ld=APDELPADirect" target="_blank" rel="noopener noreferrer">https://pay.amazon.de/help/201212490?ld=APDELPADirect</a>.
        </p>
        <h2>9. Audio- und Videokonferenzen</h2>
        <h4>Datenverarbeitung</h4>
        <p>F&uuml;r die Kommunikation mit unseren Kunden setzen wir unter anderen Online-Konferenz-Tools ein. Die im Einzelnen von uns genutzten Tools sind unten
            aufgelistet. Wenn Sie mit uns per Video- oder Audiokonferenz via Internet kommunizieren, werden Ihre personenbezogenen Daten von uns und dem Anbieter des
            jeweiligen Konferenz-Tools erfasst und verarbeitet.</p>
        <p>Die Konferenz-Tools erfassen dabei alle Daten, die Sie zur Nutzung der Tools bereitstellen/einsetzen (E-Mail-Adresse und/oder Ihre Telefonnummer). Ferner
            verarbeiten die Konferenz-Tools die Dauer der Konferenz, Beginn und Ende (Zeit) der Teilnahme an der Konferenz, Anzahl der Teilnehmer und sonstige
            &bdquo;Kontextinformationen&ldquo; im Zusammenhang mit dem Kommunikationsvorgang (Metadaten).</p>
        <p>Des Weiteren verarbeitet der Anbieter des Tools alle technischen Daten, die zur Abwicklung der Online-Kommunikation erforderlich sind. Dies umfasst
            insbesondere IP-Adressen, MAC-Adressen, Ger&auml;te-IDs, Ger&auml;tetyp, Betriebssystemtyp und -version, Client-Version, Kameratyp, Mikrofon oder
            Lautsprecher sowie die Art der Verbindung.</p>
        <p>Sofern innerhalb des Tools Inhalte ausgetauscht, hochgeladen oder in sonstiger Weise bereitgestellt werden, werden diese ebenfalls auf den Servern der
            Tool-Anbieter gespeichert. Zu solchen Inhalten z&auml;hlen insbesondere Cloud-Aufzeichnungen, Chat-/ Sofortnachrichten, Voicemails hochgeladene Fotos und
            Videos, Dateien, Whiteboards und andere Informationen, die w&auml;hrend der Nutzung des Dienstes geteilt werden.</p>
        <p>Bitte beachten Sie, dass wir nicht vollumf&auml;nglich Einfluss auf die Datenverarbeitungsvorg&auml;nge der verwendeten Tools haben. Unsere M&ouml;glichkeiten
            richten sich ma&szlig;geblich nach der Unternehmenspolitik des jeweiligen Anbieters. Weitere Hinweise zur Datenverarbeitung durch die Konferenztools
            entnehmen Sie den Datenschutzerkl&auml;rungen der jeweils eingesetzten Tools, die wir unter diesem Text aufgef&uuml;hrt haben.</p> <h4>Zweck und
            Rechtsgrundlagen</h4>
        <p>Die Konferenz-Tools werden genutzt, um mit angehenden oder bestehenden Vertragspartnern zu kommunizieren oder bestimmte Leistungen gegen&uuml;ber unseren
            Kunden anzubieten (Art. 6 Abs. 1 lit. b DSGVO). Des Weiteren dient der Einsatz der Tools der allgemeinen Vereinfachung und Beschleunigung der
            Kommunikation mit uns bzw. unserem Unternehmen (berechtigtes Interesse im Sinne von Art. 6 Abs. 1 lit. f DSGVO). Soweit eine Einwilligung abgefragt
            wurde, erfolgt der Einsatz der betreffenden Tools auf Grundlage dieser Einwilligung; die Einwilligung ist jederzeit mit Wirkung f&uuml;r die Zukunft
            widerrufbar.</p> <h4>Speicherdauer</h4>
        <p>Die unmittelbar von uns &uuml;ber die Video- und Konferenz-Tools erfassten Daten werden von unseren Systemen gel&ouml;scht, sobald Sie uns zur L&ouml;schung
            auffordern, Ihre Einwilligung zur Speicherung widerrufen oder der Zweck f&uuml;r die Datenspeicherung entf&auml;llt. Gespeicherte Cookies verbleiben auf
            Ihrem Endger&auml;t, bis Sie sie l&ouml;schen. Zwingende gesetzliche Aufbewahrungsfristen bleiben unber&uuml;hrt.</p>
        <p>Auf die Speicherdauer Ihrer Daten, die von den Betreibern der Konferenz-Tools zu eigenen Zwecken gespeichert werden, haben wir keinen Einfluss. F&uuml;r
            Einzelheiten dazu informieren Sie sich bitte direkt bei den Betreibern der Konferenz-Tools.</p> <h4>Eingesetzte Konferenz-Tools</h4>
        <p>Wir setzen folgende Konferenz-Tools ein:</p>
        <h3>Zoom</h3>
        <p>Wir nutzen Zoom. Anbieter dieses Dienstes ist die Zoom Communications Inc., San Jose, 55 Almaden Boulevard, 6th Floor, San Jose, CA 95113, USA. Details
            zur Datenverarbeitung entnehmen Sie der Datenschutzerkl&auml;rung von Zoom: <a href="https://zoom.us/de-de/privacy.html" target="_blank"
                                                                                           rel="noopener noreferrer">https://zoom.us/de-de/privacy.html</a>.</p>
        <p>Die Daten&uuml;bertragung in die USA wird auf die Standardvertragsklauseln der EU-Kommission gest&uuml;tzt. Details finden Sie hier: <a
                href="https://zoom.us/de-de/privacy.html" target="_blank" rel="noopener noreferrer">https://zoom.us/de-de/privacy.html</a>.</p>
        <h4>Abschluss eines Vertrags &uuml;ber Auftragsverarbeitung</h4>
        <p>Wir haben mit dem Anbieter von Zoom einen Vertrag zur Auftragsverarbeitung abgeschlossen und setzen die strengen Vorgaben der deutschen Datenschutzbeh&ouml;rden
            bei der Nutzung von Zoom vollst&auml;ndig um.</p>
        <h3>Skype for Business</h3>
        <p>Wir nutzen Skype for Business. Anbieter ist Skype Communications SARL, 23-29 Rives de Clausen, L-2165 Luxembourg. Details zur Datenverarbeitung entnehmen
            Sie der Datenschutzerkl&auml;rung von Skype: <a href="https://privacy.microsoft.com/de-de/privacystatement/" target="_blank" rel="noopener noreferrer">https://privacy.microsoft.com/de-de/privacystatement/</a>.
        </p>
        <h4>Abschluss eines Vertrags &uuml;ber Auftragsverarbeitung</h4>
        <p>Wir haben mit dem Anbieter von Skype for Business einen Vertrag zur Auftragsverarbeitung abgeschlossen und setzen die strengen Vorgaben der deutschen
            Datenschutzbeh&ouml;rden bei der Nutzung von Skype for Business vollst&auml;ndig um.</p>
    </div>
</main>
<!-- Hauptseite Ende -->

<footer class="footer">
    <div class="container text-center">
        <p>
            <a class="footer_link" href="https://www.instagram.com/_gelatoitaliano_/">
                <img src="img/icons/instagram_black.svg" width="16" height="16"> Instagram
            </a> &middot
            <a class="footer_link" href="https://www.facebook.com/Gelato-Italiano-100844725540830/">
                <img src="img/icons/facebook_black.svg" width="16" height="16"> Facebook
            </a> &middot
        </p>
        <p>
            <a class="footer_link" href="javascript:print()">
                <img src="img/icons/printer.svg"> Seite drucken
            </a> &middot
            <a class="footer_link" href="mailto:?subject=DAS%20k%C3%B6nnte%20dich%20interessieren&body=Hey%2C%20schau%20doch%20mal%20bei%20Gelato%20Italiano%20vorbei%2C%20das%20k%C3%B6nnte%20interessant%20f%C3%BCr%20dich%20sein!%0A%0Ahttps%3A%2F%2Fgelato-italiano.de">
                <img src="img/icons/chat-left.svg"> Seite weiterempfehlen
            </a> &middot
        </p>

        <p>
            <a class="footer_link" href="index.php">Home</a> &middot;
            <a class="footer_link" href="eissorten.php">Unsere Eissorten</a> &middot;
            <a class="footer_link" href="standort.php">Der Standort</a> &middot;
            <a class="footer_link active" href="datenschutz.php">Datenschutz</a> &middot;
            <a class="footer_link" href="impressum.php">Impressum</a> &middot;
            <a class="footer_link" href="login.php">Login</a>
        </p>

        <p style="margin-bottom: 0">&copy; 2021 Gelato Italiano Bernd Klaus</p> <!--TODO: -->
    </div>
</footer>

</body>

<!-- Skripte -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</html>