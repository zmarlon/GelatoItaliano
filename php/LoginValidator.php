<?php

define("GI_SUCCESS", 0);
define("GI_INVALID_EMAIL", -1);
define("GI_INVALID_PASSWORD", -2);

class LoginValidator {
    private string $mail;
    private string $hash;

    public function __construct() {
        $json = json_decode(file_get_contents("data/user.json"), true);
        $this->mail = $json['Email'];
        $this->hash = $json['PasswordHash'];
    }

    public function isValidUser(string $mail, string $password) : int {
        if($this->mail != $mail) return GI_INVALID_EMAIL;
        return password_verify($password, $this->hash) ? GI_SUCCESS : GI_INVALID_PASSWORD;
    }
}