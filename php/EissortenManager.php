<?php

include_once "LoginValidator.php";

class EissortenManager {
    private LoginValidator $validator;

    public function __construct() {
        $this->validator = new LoginValidator();
    }

    public function createEissortenShow(): string {
        $json = json_decode(file_get_contents("data/eissorten.json"), true);
        $eissorten = $json['Eissorten'];

        $str = "<div class=\"container\"><div class=\"px-lg-5\"><div class=\"row\">";

        $index = 0;
        foreach($eissorten as $sorte) {
            $name = $sorte['Name'];
            $hersteller = $sorte['Hersteller'];
            $thumbnail = $sorte['Thumbnail'];

            $str .= "<div class=\"col-xl-3 col-lg-4 col-md-6 mb-4\"><div class=\"fk_bg_light_gray rounded shadow-sm\">
            <img class=\"img-fluid card-img-top\" src=\"img/eissorten/$thumbnail\" alt=\"$thumbnail\" width='319' height='202'>
            <div class=\"p-4\"><h5 class=\"text-dark\">$name</h5>
            <p class=\"small text-muted mb-0\">$hersteller</p></div></div></div>";


            $index++;
        }

        $str .= "</div></div></div>";

        return $str;
    }

    public function createModals(): string {
        $json = json_decode(file_get_contents("data/eissorten.json"), true);
        $eissorten = $json['Eissorten'];

        $str = "";

        $index = 0;
        foreach($eissorten as $sorte) {
            $sortenName = $eissorten[$index]['Name'];

            $str .= "<div class=\"modal fade\" id=\"modal$index\" tabindex=\"-1\" aria-labelledby=\"modalLabel$index\" aria-hidden=\"true\">
  <div class=\"modal-dialog\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\" id=\"modalLabel$index\">Eissorte vor $sortenName einfügen</h5>
        <button type=\"button\" class=\"btn-close\" data-bs-dismiss=\"modal\" aria-label=\"Close\"></button>
      </div>
      <div class=\"modal-body\">
        <div class=\"text-center\">
            <form id=\"eissorteForm\" action=\"eissorten_manager_upload.php\" method=\"post\" enctype=\"multipart/form-data\">
                <input type=\"text\" class=\"form-control mb-3\" name=\"eissorteName\" id=\"eissorteName\" aria-describedby=\"modalNameDescriptionProperty$index\" placeholder=\"Name der Sorte\">
                <input type=\"text\" class=\"form-control mb-3\" name=\"eissorteHersteller\" id=\"eissorteHersteller\" aria-describedby=\"modalHerstellerDescriptionProperty$index\" placeholder=\"Hersteller\">
                <input type=\"hidden\" name=\"eisindex\" value=\"$index\" />
            
                <input type=\"file\" class=\"form-control\" id=\"filesToUpload\" name=\"filesToUpload\">
                <input type=\"submit\" value=\"Upload Image\" name=\"submit\">
               
            </form>
        </div>
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-danger\" data-bs-dismiss=\"modal\">Abbrechen</button>
        <button type=\"button\" class=\"btn btn-success\">Hinzufügen</button>
      </div>
    </div>
  </div>
</div>";
            $index++;
        }

        $str .= "<div class=\"modal fade\" id=\"modal-1\" tabindex=\"-1\" aria-labelledby=\"modalLabel-1\" aria-hidden=\"true\">
  <div class=\"modal-dialog\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\" id=\"modalLabel-1\">Eissorte am Ende einfügen</h5>
        <button type=\"button\" class=\"btn-close\" data-bs-dismiss=\"modal\" aria-label=\"Close\"></button>
      </div>
      <div class=\"modal-body\">
        <div class=\"text-center\">
            <form action=\"eissorten_manager_upload.php\" name=\"eissorteForm\" method=\"post\" enctype=\"multipart/form-data\" onsubmit='return validateEissorteUpload();'>
                <input type=\"text\" class=\"form-control mb-3\" name=\"eissorteName\" id=\"eissorteName\" aria-describedby=\"modalNameDescriptionProperty-1\" placeholder=\"Name der Sorte\">
                <input type=\"text\" class=\"form-control mb-3\" name=\"eissorteHersteller\" id=\"eissorteHersteller\" aria-describedby=\"modalHerstellerDescriptionProperty-1\" placeholder=\"Hersteller\">
                <input type=\"hidden\" name=\"eisindex\" value=\"-1\" />
            
                <input type=\"file\" class=\"form-control\" id=\"filesToUpload\" name=\"filesToUpload\">
                <input type=\"submit\" value=\"Upload Image\" name=\"submit\">
               
            </form>
        </div>
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-danger\" data-bs-dismiss=\"modal\">Abbrechen</button>
        <button type=\"button\" class=\"btn btn-success\">Hinzufügen</button>
      </div>
    </div>
  </div>
</div>";

        return $str;
    }

    public function createEissortenTable(): string {
        $json = json_decode(file_get_contents("data/eissorten.json"), true);
        $eissorten = $json['Eissorten'];

        $str = '<table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Name</th>
                            <th scope="col">Hersteller</th>
                            <th scope="col">Thumbnail</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>';

        $index = 0;
        foreach($eissorten as $sorte) {
            $name = $sorte['Name'];
            $hersteller = $sorte['Hersteller'];
            $thumbnail = $sorte['Thumbnail'];

            $str .= "<tr>
                <td>
                    <button type=\"button\" class=\"btn btn-success btn-sm\" data-bs-toggle=\"modal\" data-bs-target=\"#modal$index\">+</button>                    
                </td>
                <th scope=\"row\">$name</th><td>$hersteller</td><td>$thumbnail</td>
                <td>
                    <a class=\"btn btn-danger btn-sm\" href=\"eissorten_manager.php?remove_sorte=$index\">-</a>
                </td>
            </tr>";

            $index++;
        }

        $str .= '</tbody></table>';

        return $str;
    }

    public function removeSorte(int $index) {
        $json = json_decode(file_get_contents("data/eissorten.json"), true);

        $thumbnailPath = "img/eissorten/" . $json['Eissorten'][$index]["Thumbnail"];

        if(file_exists($thumbnailPath)) {
            unlink($thumbnailPath);
        }

        $oldEissorten = $json['Eissorten'];
        $newEissorten = array();

        $ix = 0;
        foreach ($oldEissorten as $eissorte) {
            if($ix != $index) {
                array_push($newEissorten, $eissorte);
            }
            $ix++;
        }

        $json['Eissorten'] = $newEissorten;
        $json_string = json_encode($json, JSON_PRETTY_PRINT);

        file_put_contents("data/eissorten.json", $json_string);
    }

    public function getValidator(): LoginValidator {
        return $this->validator;
    }
}