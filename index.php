<!doctype html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/icons/gelatoitaliano.ico">

    <title>Gelato Italiano</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/gelatoitaliano.css">
</head>

<body>

<div class="fixed-top text-center">
    <div class="location-info pt-2 pb-2">
        <a class="link-light" href="https://goo.gl/maps/nY2AdR9GFTTk67Dp6">Du findest uns hier: Fliehburgstraße 15, 56856 Zell</a>
    </div>

    <nav class="navbar navbar-expand-lg navbar-light bg-light shadow-sm pt-3 mb-5 bg-white rounded aria-label="Navbar">
        <div class="container">
            <a class="navbar-brand" href="index.php">
                <img src="img/logo.svg" alt="Logo" width="250" height="135">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#gelatoitalianoNav" aria-controls="gelatoitalianoNav" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- NavBar Items -->
            <div class="collapse navbar-collapse" id="gelatoitalianoNav">
                <div class="nav-collapse-properties">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="eissorten.php">Unsere Eissorten</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="standort.php">Der Standort</a>
                        </li>
                    </ul>
                </div>
            </div>

            <!-- Social Media logos -->
            <div class="colored-social-media-icon d-none d-xl-block d-lg-block padding-insta-logo">
                <a href="https://www.instagram.com/_gelatoitaliano_/">
                    <img class="p-2" src="img/icons/instagram_white.svg" alt="Instagram" width="32" height="32">
                </a>
            </div>

            <div class="colored-social-media-icon d-none d-xl-block d-lg-block">
                <a href="https://www.facebook.com/Gelato-Italiano-100844725540830/">
                    <img class="p-2" src="img/icons/facebook_white.svg" alt="Facebook" width="32" height="32">
                </a>
            </div>
        </div>
    </nav>
</div>

<!-- Hauptseite -->
<main role="main">
    <div id="mainCarousel" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#mainCarousel" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#mainCarousel" data-bs-slide-to="1" aria-label="Slide 2"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="img/main_img1.jpg" class="d-block w-100" alt="Hintergrundbild 2">
            </div>
            <div class="carousel-item">
                <img src="img/main_img2.jpg" class="d-block w-100" alt="Hintergrundbild 3">
            </div>
        </div>
        <div class="carousel-caption">
            <p><a class="btn btn-lg btn-light mb-5" href="eissorten.php">Zu den Eissorten</a></p>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#mainCarousel" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#mainCarousel" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
</main>
<!-- Hauptseite Ende -->

<footer class="footer">
    <div class="container text-center">
        <p>
            <a class="footer_link" href="https://www.instagram.com/_gelatoitaliano_/">
                <img src="img/icons/instagram_black.svg" width="16" height="16"> Instagram
            </a> &middot
            <a class="footer_link" href="https://www.facebook.com/Gelato-Italiano-100844725540830/">
                <img src="img/icons/facebook_black.svg" width="16" height="16"> Facebook
            </a> &middot
        </p>
        <p>
            <a class="footer_link" href="javascript:print()">
                <img src="img/icons/printer.svg"> Seite drucken
            </a> &middot
            <a class="footer_link" href="mailto:?subject=DAS%20k%C3%B6nnte%20dich%20interessieren&body=Hey%2C%20schau%20doch%20mal%20bei%20Gelato%20Italiano%20vorbei%2C%20das%20k%C3%B6nnte%20interessant%20f%C3%BCr%20dich%20sein!%0A%0Ahttps%3A%2F%2Fgelato-italiano.de">
                <img src="img/icons/chat-left.svg"> Seite weiterempfehlen
            </a> &middot
        </p>

        <p>
            <a class="footer_link active" href="index.php">Home</a> &middot;
            <a class="footer_link" href="eissorten.php">Unsere Eissorten</a> &middot;
            <a class="footer_link" href="standort.php">Der Standort</a> &middot;
            <a class="footer_link" href="datenschutz.php">Datenschutz</a> &middot;
            <a class="footer_link" href="impressum.php">Impressum</a> &middot;
            <a class="footer_link" href="login.php">Login</a>
        </p>

        <p style="margin-bottom: 0">&copy; 2021 Gelato Italiano Bernd Klaus</p> <!--TODO: -->
    </div>
</footer>

</body>

<!-- Skripte -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</html>