<!doctype html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/icons/gelatoitaliano.ico">

    <title>Gelato Italiano - Standort</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/gelatoitaliano.css">
    <link rel="stylesheet" href="css/gelatoitaliano-standort.css">
</head>

<body>

<div class="fixed-top text-center">
    <div class="location-info pt-2 pb-2">
        <a class="link-light" href="https://goo.gl/maps/nY2AdR9GFTTk67Dp6">Du findest uns hier: Fliehburgstraße 15, 56856 Zell</a>
    </div>

    <nav class="navbar navbar-expand-lg navbar-light bg-light shadow-sm pt-3 mb-5 bg-white rounded aria-label="Navbar">
    <div class="container">
        <a class="navbar-brand" href="index.php">
            <img src="img/logo.svg" alt="Logo" width="250" height="135">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#gelatoitalianoNav" aria-controls="gelatoitalianoNav" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- NavBar Items -->
        <div class="collapse navbar-collapse" id="gelatoitalianoNav">
            <div class="nav-collapse-properties">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="eissorten.php">Unsere Eissorten</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="standort.php">Der Standort</a>
                    </li>
                </ul>
            </div>
        </div>

        <!-- Social Media logos -->
        <div class="colored-social-media-icon d-none d-xl-block d-lg-block padding-insta-logo">
            <a href="https://www.instagram.com/_gelatoitaliano_/">
                <img class="p-2" src="img/icons/instagram_white.svg" alt="Instagram" width="32" height="32">
            </a>
        </div>

        <div class="colored-social-media-icon d-none d-xl-block d-lg-block">
            <a href="https://www.facebook.com/Gelato-Italiano-100844725540830/">
                <img class="p-2" src="img/icons/facebook_white.svg" alt="Facebook" width="32" height="32">
            </a>
        </div>
    </div>
    </nav>
</div>

<!-- Hauptseite -->
<main role="main">
    <div class="google-maps-karte">
        <iframe height="500"
                src="https://maps.google.com/maps?q=Fliehburgstra%C3%9Fe%2015,%2056856%20Zell%20(Mosel)&t=&z=13&ie=UTF8&iwloc=&output=embed"
                frameborder="0" scrolling="no" marginheight="0" marginwidth="0" style='height:500px; width:100%;'>
        </iframe>
    </div>
    <div class="container text-center">
        <h1 class="pt-5 pb-3" style="color: var(--gelatoitaliano-text-col); font-family: Calibri, serif">Unser Standort</h1>
        <h3>Sie wollen wissen, welche Eissorten wir momentan anbieten?</h3>
        <a href="eissorten.php" class="btn btn-dark">Zu den Eissorten</a>
    </div>
</main>
<!-- Hauptseite Ende -->

<footer class="footer">
    <div class="container text-center">
        <p>
            <a class="footer_link" href="https://www.instagram.com/_gelatoitaliano_/">
                <img src="img/icons/instagram_black.svg" width="16" height="16"> Instagram
            </a> &middot
            <a class="footer_link" href="https://www.facebook.com/Gelato-Italiano-100844725540830/">
                <img src="img/icons/facebook_black.svg" width="16" height="16"> Facebook
            </a> &middot
        </p>
        <p>
            <a class="footer_link" href="javascript:print()">
                <img src="img/icons/printer.svg"> Seite drucken
            </a> &middot
            <a class="footer_link" href="mailto:?subject=DAS%20k%C3%B6nnte%20dich%20interessieren&body=Hey%2C%20schau%20doch%20mal%20bei%20Gelato%20Italiano%20vorbei%2C%20das%20k%C3%B6nnte%20interessant%20f%C3%BCr%20dich%20sein!%0A%0Ahttps%3A%2F%2Fgelato-italiano.de">
                <img src="img/icons/chat-left.svg"> Seite weiterempfehlen
            </a> &middot
        </p>

        <p>
            <a class="footer_link" href="index.php">Home</a> &middot;
            <a class="footer_link" href="eissorten.php">Unsere Eissorten</a> &middot;
            <a class="footer_link active" href="standort.php">Der Standort</a> &middot;
            <a class="footer_link" href="datenschutz.php">Datenschutz</a> &middot;
            <a class="footer_link" href="impressum.php">Impressum</a> &middot;
            <a class="footer_link" href="login.php">Login</a>
        </p>

        <p style="margin-bottom: 0">&copy; 2021 Gelato Italiano Bernd Klaus</p> <!--TODO: -->
    </div>
</footer>

</body>

<!-- Skripte -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</html>