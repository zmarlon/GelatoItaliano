<?php

include_once "php/ArrayHelper.php";

$target_dir = "img/eissorten/";
$target_file = $target_dir . basename($_FILES["filesToUpload"]["name"]);
$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

if($_FILES["filesToUpload"]["tmp_name"] == null || $_FILES["filesToUpload"]["tmp_name"] == "") {
    header("Location: eissorten_manager.php?file_not_selected");
    return;
}

if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["filesToUpload"]["tmp_name"]);
    if($check == false) {
        header("Location: eissorten_manager.php?file_unknown_error");
        return;
    }
}

if(file_exists($target_file)) {
    header("Location: eissorten_manager.php?file_does_exist");
    return;
}

if($_FILES["filesToUpload"]["size"] > 5000000) {
    header("Location: eissorten_manager.php?file_is_too_large");
    return;
}

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
    header("Location: eissorten_manager.php?file_format_wrong");
    return;
}

if (move_uploaded_file($_FILES["filesToUpload"]["tmp_name"], $target_file)) {
    if(isset($_POST["eissorteName"]) && isset($_POST["eissorteHersteller"]) && isset($_POST["eisindex"])) {
        if($_POST["eissorteName"] == "" || $_POST["eissorteHersteller"] == ""){
            header("Location: eissorten_manager.php?no_name_or_no_creator");
            return;
        }

        $json = json_decode(file_get_contents("data/eissorten.json"), true);
        $eissorten = $json['Eissorten'];

        $oldEissorten = $json['Eissorten'];
        $newEissorten = array();

        $ix = 0;

        $indexToAdd = (int)$_POST["eisindex"];

        foreach ($oldEissorten as $eissorte) {
            array_push($newEissorten, $eissorte);
            $ix++;
        }

        //add new
        $newElement = array("Name" => $_POST["eissorteName"], "Hersteller" => $_POST["eissorteHersteller"], "Thumbnail" => $_FILES["filesToUpload"]["name"]);
        if($indexToAdd != -1) {
            ArrayHelper::insertValueAtPos($newEissorten, $indexToAdd, $newElement);
        } else {
            array_push($newEissorten, $newElement);
        }

        $json['Eissorten'] = $newEissorten;
        $json_string = json_encode($json, JSON_PRETTY_PRINT);

        file_put_contents("data/eissorten.json", $json_string);

        header("Location: eissorten_manager.php");
    } else {
        header("Location: eissorten_manager.php?file_unknown_error");
    }
} else {
    header("Location: eissorten_manager.php?file_unknown_error");
}