<!doctype html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/icons/gelatoitaliano.ico">

    <title>Gelato Italiano - Impressum</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/gelatoitaliano.css">
</head>

<body>

<div class="fixed-top text-center">
    <div class="location-info pt-2 pb-2">
        <a class="link-light" href="https://goo.gl/maps/nY2AdR9GFTTk67Dp6">Du findest uns hier: Fliehburgstraße 15, 56856 Zell</a>
    </div>

    <nav class="navbar navbar-expand-lg navbar-light bg-light shadow-sm pt-3 mb-5 bg-white rounded aria-label="Navbar">
    <div class="container">
        <a class="navbar-brand" href="index.php">
            <img src="img/logo.svg" alt="Logo" width="250" height="135">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#gelatoitalianoNav" aria-controls="gelatoitalianoNav" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- NavBar Items -->
        <div class="collapse navbar-collapse" id="gelatoitalianoNav">
            <div class="nav-collapse-properties">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="eissorten.php">Unsere Eissorten</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="standort.php">Der Standort</a>
                    </li>
                </ul>
            </div>
        </div>

        <!-- Social Media logos -->
        <div class="colored-social-media-icon d-none d-xl-block d-lg-block padding-insta-logo">
            <a href="https://www.instagram.com/_gelatoitaliano_/">
                <img class="p-2" src="img/icons/instagram_white.svg" alt="Instagram" width="32" height="32">
            </a>
        </div>

        <div class="colored-social-media-icon d-none d-xl-block d-lg-block">
            <a href="https://www.facebook.com/Gelato-Italiano-100844725540830/">
                <img class="p-2" src="img/icons/facebook_white.svg" alt="Facebook" width="32" height="32">
            </a>
        </div>
    </div>
    </nav>
</div>

<!-- Hauptseite -->
<main role="main">
    <div class="container pt-4 pb-4">
        <h1 class="text-center pb-2">Impressum</h1>

        <h2>Angaben gem&auml;&szlig; &sect; 5 TMG</h2>
        <p>Bernd Klaus<br/>
            Gelato Italiano Bernd Klaus<br/>
            Auf der Purth 28<br/>
            56859 Bullay</p>

        <h2>Kontakt</h2>
        <p>Telefon: 0175-5116741<br/>
            E-Mail: gelato-italiano@outlook.de</p>

        <h2>Redaktionell Verantwortlicher</h2>
        <p>Gelato Italiano Bernd Klaus<br/>
            Auf der Purth 28<br/>
            56859 Bullay</p>

        <h2>EU-Streitschlichtung</h2>
        <p>Die Europ&auml;ische Kommission stellt eine Plattform zur Online-Streitbeilegung (OS) bereit: <a href="https://ec.europa.eu/consumers/odr/"
                                                                                                            target="_blank" rel="noopener noreferrer">https://ec.europa.eu/consumers/odr/</a>.<br/>
            Unsere E-Mail-Adresse finden Sie oben im Impressum.</p>

        <h2>Verbraucher&shy;streit&shy;beilegung/Universal&shy;schlichtungs&shy;stelle</h2>
        <p>Wir sind nicht bereit oder verpflichtet, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.</p>

        <h3>Haftung f&uuml;r Inhalte</h3>
        <p>Als Diensteanbieter sind wir gem&auml;&szlig; &sect; 7 Abs.1 TMG f&uuml;r eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich.
            Nach &sect;&sect; 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, &uuml;bermittelte oder gespeicherte fremde Informationen zu &uuml;berwachen
            oder nach Umst&auml;nden zu forschen, die auf eine rechtswidrige T&auml;tigkeit hinweisen.</p>
        <p>Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unber&uuml;hrt. Eine diesbez&uuml;gliche
            Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung m&ouml;glich. Bei Bekanntwerden von entsprechenden
            Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.</p>
        <h3>Haftung f&uuml;r Links</h3>
        <p>Unser Angebot enth&auml;lt Links zu externen Websites Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb k&ouml;nnen wir f&uuml;r diese fremden
            Inhalte auch keine Gew&auml;hr &uuml;bernehmen. F&uuml;r die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten
            verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf m&ouml;gliche Rechtsverst&ouml;&szlig;e &uuml;berpr&uuml;ft. Rechtswidrige
            Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar.</p>
        <p>Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei
            Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.</p>
        <h3>Urheberrecht</h3>
        <p>Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielf&auml;ltigung,
            Bearbeitung, Verbreitung und jede Art der Verwertung au&szlig;erhalb der Grenzen des Urheberrechtes bed&uuml;rfen der schriftlichen Zustimmung des
            jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur f&uuml;r den privaten, nicht kommerziellen Gebrauch gestattet.</p>
        <p>Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter
            als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei
            Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.</p>


    </div>
</main>
<!-- Hauptseite Ende -->

<footer class="footer">
    <div class="container text-center">
        <p>
            <a class="footer_link" href="https://www.instagram.com/_gelatoitaliano_/">
                <img src="img/icons/instagram_black.svg" width="16" height="16"> Instagram
            </a> &middot
            <a class="footer_link" href="https://www.facebook.com/Gelato-Italiano-100844725540830/">
                <img src="img/icons/facebook_black.svg" width="16" height="16"> Facebook
            </a> &middot
        </p>
        <p>
            <a class="footer_link" href="javascript:print()">
                <img src="img/icons/printer.svg"> Seite drucken
            </a> &middot
            <a class="footer_link" href="mailto:?subject=DAS%20k%C3%B6nnte%20dich%20interessieren&body=Hey%2C%20schau%20doch%20mal%20bei%20Gelato%20Italiano%20vorbei%2C%20das%20k%C3%B6nnte%20interessant%20f%C3%BCr%20dich%20sein!%0A%0Ahttps%3A%2F%2Fgelato-italiano.de">
                <img src="img/icons/chat-left.svg"> Seite weiterempfehlen
            </a> &middot
        </p>

        <p>
            <a class="footer_link" href="index.php">Home</a> &middot;
            <a class="footer_link" href="eissorten.php">Unsere Eissorten</a> &middot;
            <a class="footer_link" href="standort.php">Der Standort</a> &middot;
            <a class="footer_link" href="datenschutz.php">Datenschutz</a> &middot;
            <a class="footer_link active" href="impressum.php">Impressum</a> &middot;
            <a class="footer_link" href="login.php">Login</a>
        </p>

        <p style="margin-bottom: 0">&copy; 2021 Gelato Italiano Bernd Klaus</p> <!--TODO: -->
    </div>
</footer>

</body>

<!-- Skripte -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</html>