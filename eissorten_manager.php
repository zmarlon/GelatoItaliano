<?php

session_start();

if(!isset($_SESSION['email'])) {
    header("Location: login.php");
    return;
}

if(isset($_GET['logout'])) {
    session_destroy();
    header("Location: login.php");
    return;
}

include_once "php/EissortenManager.php";

$manager = new EissortenManager();

if(isset($_GET['remove_sorte'])) {
    $manager->removeSorte($_GET['remove_sorte']);
    header("Location: eissorten_manager.php");
}

?>

<!doctype html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/icons/gelatoitaliano.ico">

    <title>Gelato Italiano - Eissorten Manager</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/gelatoitaliano.css">
</head>

<body>

<?php
    echo $manager->createModals();
?>

<div class="fixed-top text-center">
    <div class="location-info pt-2 pb-2">
        <a class="link-light" href="https://goo.gl/maps/nY2AdR9GFTTk67Dp6">Du findest uns hier: Fliehburgstraße 15, 56856 Zell</a>
    </div>

    <nav class="navbar navbar-expand-lg navbar-light bg-light shadow-sm pt-3 mb-5 bg-white rounded aria-label="Navbar">
    <div class="container">
        <a class="navbar-brand" href="index.php">
            <img src="img/logo.svg" alt="Logo" width="250" height="135">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#gelatoitalianoNav" aria-controls="gelatoitalianoNav" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- NavBar Items -->
        <div class="collapse navbar-collapse" id="gelatoitalianoNav">
            <div class="nav-collapse-properties">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="eissorten.php">Unsere Eissorten</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="standort.php">Der Standort</a>
                    </li>
                </ul>
            </div>
        </div>

        <!-- Social Media logos -->
        <div class="colored-social-media-icon d-none d-xl-block d-lg-block padding-insta-logo">
            <a href="https://www.instagram.com/_gelatoitaliano_/">
                <img class="p-2" src="img/icons/instagram_white.svg" alt="Instagram" width="32" height="32">
            </a>
        </div>

        <div class="colored-social-media-icon d-none d-xl-block d-lg-block">
            <a href="https://www.facebook.com/Gelato-Italiano-100844725540830/">
                <img class="p-2" src="img/icons/facebook_white.svg" alt="Facebook" width="32" height="32">
            </a>
        </div>
    </div>
    </nav>
</div>

<!-- Hauptseite -->
<main role="main">
    <div class="container pt-4 pb-4 text-center">
        <h3 class="pb-2">Sie sind angemeldet als: <?php echo $_SESSION['email'] ?></h3>
        <h3 class="pb-2">Verfügbare Eissorten:</h3>

        <?php
            echo $manager->createEissortenTable(); ?>

        <button type="button mb-2" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modal-1">Am Ende hinzufügen</button>

        <?php

        if(isset($_GET['file_unknown_error'])) {
            echo "<h3 class=\"pb-2\" style='color: red;'>Das Bild der Eissorte konnte nicht hochgeladen werden. Bitte versuchen Sie es erneut!</h3>";
        }

        if(isset($_GET['file_does_exist'])) {
            echo "<h3 class=\"pb-2\" style='color: red;'>Das Bild der Eissorte konnte nicht hochgeladen werden. Die ausgewählte Datei existiert bereits. Lade die Datei unter einem anderen Dateinamen hoch!</h3>";
        }

        if(isset($_GET['file_is_too_large'])) {
            echo "<h3 class=\"pb-2\" style='color: red;'>Das Bild der Eissorte konnte nicht hochgeladen werden. Die ausgewählte Datei darf nicht größer als 5MB sein!</h3>";
        }

        if(isset($_GET['file_format_wrong'])) {
            echo "<h3 class=\"pb-2\" style='color: red;'>Das Bild der Eissorte konnte nicht hochgeladen werden. Falsches Dateiformat. Erlaubte Formate: JPG, PNG, GIF!</h3>";
        }

        if(isset($_GET['file_not_selected'])) {
            echo "<h3 class=\"pb-2\" style='color: red;'>Das Bild der Eissorte konnte nicht hochgeladen werden. Es wurde keine Bilddatei ausgewählt!</h3>";
        }

        if(isset($_GET['no_name_or_no_creator'])) {
            echo "<h3 class=\"pb-2\" style='color: red;'>Das Bild der Eissorte konnte nicht hochgeladen werden. Bitte gebe einen Namen und einen Hersteller an!</h3>";
        }
        ?>

        <form action="?logout" method="post">
            <input class="btn btn-lg btn-outline-dark" type="submit" value="Abmelden">
        </form>
    </div>
</main>
<!-- Hauptseite Ende -->

<footer class="footer">
    <div class="container text-center">
        <p>
            <a class="footer_link" href="https://www.instagram.com/_gelatoitaliano_/">
                <img src="img/icons/instagram_black.svg" width="16" height="16"> Instagram
            </a> &middot
            <a class="footer_link" href="https://www.facebook.com/Gelato-Italiano-100844725540830/">
                <img src="img/icons/facebook_black.svg" width="16" height="16"> Facebook
            </a> &middot
        </p>
        <p>
            <a class="footer_link" href="javascript:print()">
                <img src="img/icons/printer.svg"> Seite drucken
            </a> &middot
            <a class="footer_link" href="mailto:?subject=DAS%20k%C3%B6nnte%20dich%20interessieren&body=Hey%2C%20schau%20doch%20mal%20bei%20Gelato%20Italiano%20vorbei%2C%20das%20k%C3%B6nnte%20interessant%20f%C3%BCr%20dich%20sein!%0A%0Ahttps%3A%2F%2Fgelato-italiano.de">
                <img src="img/icons/chat-left.svg"> Seite weiterempfehlen
            </a> &middot
        </p>

        <p>
            <a class="footer_link" href="index.php">Home</a> &middot;
            <a class="footer_link" href="eissorten.php">Unsere Eissorten</a> &middot;
            <a class="footer_link" href="standort.php">Der Standort</a> &middot;
            <a class="footer_link" href="datenschutz.php">Datenschutz</a> &middot;
            <a class="footer_link" href="impressum.php">Impressum</a> &middot;
            <a class="footer_link active" href="login.php">Login</a>
        </p>

        <p style="margin-bottom: 0">&copy; 2021 Gelato Italiano Bernd Klaus</p> <!--TODO: -->
    </div>
</footer>

</body>

<!-- Skripte -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</html>